﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="IndustryProject3A.Admin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%-- Inline Style --%>
    <style>
        .modalBackground {
            background-color: black;
            filter: alpha(opacity=0.9) !important;
            opacity: 0.6 !important;
            z-index: 20;
        }

        .modalPopup {
            padding: 20px 10px 24px 10px;
            position: relative;
            width: auto;
            height: auto;
            background-color: #004A80;
            border: 1px solid black;
            top: 0px;
            left: 1px;
        }

        .modalBackdrop {
            z-index: -1;
        }

        .errorText {
            padding: 2px 2px 2px 2px;
            position: relative;
            background-color: lightcoral;
            border: 1px solid red;
        }
    </style>

    <%-- Title Image --%>
    <div class="Jumbotron">
        <br />
        <img src="Images/Banner.png" />
    </div>

    <%-- Page Title --%>
    <div class="col-md-11">
        <center><h2>Admin Settings</h2></center>
        <center>
            <table cellspacing="10" cellpadding="10">
            <tr>
                <td valign="top">
                    <center>
            <h3>Instance Manager Settings:</h3> 
        <asp:Label ID="Label_MinLoad" runat="server" Text="Minimum Load: "></asp:Label><asp:TextBox ID="TextBox_MinLoad" runat="server"></asp:TextBox>
        <asp:Label ID="Label_MaxLoad" runat="server" Text="Maximum Load: "></asp:Label><asp:TextBox ID="TextBox_MaxLoad" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label_NoLecturers" runat="server" Text="No Lecturer"></asp:Label>
            <br /><asp:TextBox ID="TextBox_NoLecturer_Colour" autocomplete="off" TabIndex="-1" onkeydown="return false;" onkeypress="return false;" onkeyup="return false;" runat="server"></asp:TextBox><ajaxToolkit:ColorPickerExtender ID="ColorPicker_NoLecturers" runat="server" TargetControlID="TextBox_NoLecturer_Colour" SampleControlID="TextBox_NoLecturer_Colour" />
        <br />
        <asp:Label ID="Label_NoPrimaryLecturer" runat="server" Text="No Primary Lecturer"></asp:Label>
            <br /><asp:TextBox ID="TextBox_NoPrimaryLecturer_Colour" autocomplete="off" TabIndex="-1" onkeydown="return false;" onkeypress="return false;" onkeyup="return false;" runat="server"></asp:TextBox><ajaxToolkit:ColorPickerExtender ID="ColorPicker_NoPrimaryLecturer" runat="server" TargetControlID="TextBox_NoPrimaryLecturer_Colour" SampleControlID="TextBox_NoPrimaryLecturer_Colour" />
            <br />
        <asp:Label ID="Label_NoIssues" runat="server" Text="No Issues"></asp:Label>
            <br /><asp:TextBox ID="TextBox_NoIssues_Colour" autocomplete="off" TabIndex="-1" onkeydown="return false;" onkeypress="return false;" onkeyup="return false;" runat="server"></asp:TextBox><ajaxToolkit:ColorPickerExtender ID="ColorPicker_NoIssues" runat="server" TargetControlID="TextBox_NoIssues_Colour" SampleControlID="TextBox_NoIssues_Colour" />
            <br />
        <asp:Label ID="Label_AnnualConflict" runat="server" Text="Annual Leave Conflict"></asp:Label>
            <br /><asp:TextBox ID="TextBox_AnnualConflict_Colour" autocomplete="off" TabIndex="-1" onkeydown="return false;" onkeypress="return false;" onkeyup="return false;" runat="server"></asp:TextBox><ajaxToolkit:ColorPickerExtender ID="ColorPicker_AnnualConflict" runat="server" TargetControlID="TextBox_AnnualConflict_Colour" SampleControlID="TextBox_AnnualConflict_Colour" />
            <br />
        <asp:Label ID="Label_LecturerUnderUtilise" runat="server" Text="Lecturer Under Utilised"></asp:Label>
            <br /><asp:TextBox ID="TextBox_LecturerUnderUtilise_Colour" autocomplete="off" TabIndex="-1" onkeydown="return false;" onkeypress="return false;" onkeyup="return false;" runat="server"></asp:TextBox><ajaxToolkit:ColorPickerExtender ID="ColorPicker_LecturerUnderUtilise" runat="server" TargetControlID="TextBox_LecturerUnderUtilise_Colour" SampleControlID="TextBox_LecturerUnderUtilise_Colour" />
            <br />
        <asp:Label ID="Label_LecturerGoodUtilise" runat="server" Text="Lecturer Well Utilised"></asp:Label>
            <br /><asp:TextBox ID="TextBox_LecturerGoodUtilise_Colour" autocomplete="off" TabIndex="-1" onkeydown="return false;" onkeypress="return false;" onkeyup="return false;" runat="server"></asp:TextBox><ajaxToolkit:ColorPickerExtender ID="ColorPicker_LecturerGoodUtilise" runat="server" TargetControlID="TextBox_LecturerGoodUtilise_Colour" SampleControlID="TextBox_LecturerGoodUtilise_Colour" />
            <br />
        <asp:Label ID="Label_LecturerOverUtilise" runat="server" Text="Lecturer Over Utilised"></asp:Label>
            <br /><asp:TextBox ID="TextBox_LecturerOverUtilise_Colour" autocomplete="off" TabIndex="-1" onkeydown="return false;" onkeypress="return false;" onkeyup="return false;" runat="server"></asp:TextBox><ajaxToolkit:ColorPickerExtender ID="ColorPicker_LecturerOverUtilise" runat="server" TargetControlID="TextBox_LecturerOverUtilise_Colour" SampleControlID="TextBox_LecturerOverUtilise_Colour" />
              <br />
                        </center>
                </td>
                <td valign="top">
                    <center>
                <h3>Lecturer Calendar Settings:</h3> 
<asp:Label ID="Label_teachLead" runat="server" Text="Teaching Lead"></asp:Label>
            <br /><asp:TextBox ID="TextBox_teachLead" autocomplete="off" TabIndex="-1" onkeydown="return false;" onkeypress="return false;" onkeyup="return false;" runat="server"></asp:TextBox><ajaxToolkit:ColorPickerExtender ID="ColorPicker_teachLead" runat="server" TargetControlID="TextBox_teachLead" SampleControlID="TextBox_teachLead" />
        <br />
        <asp:Label ID="Label_teachSupport" runat="server" Text="Teaching Support"></asp:Label>
            <br /><asp:TextBox ID="TextBox_teachSupport" autocomplete="off" TabIndex="-1" onkeydown="return false;" onkeypress="return false;" onkeyup="return false;" runat="server"></asp:TextBox><ajaxToolkit:ColorPickerExtender ID="ColorPicker_teachSupport" runat="server" TargetControlID="TextBox_teachSupport" SampleControlID="TextBox_teachSupport" />
            <br />
        <asp:Label ID="Label_dev" runat="server" Text="Developing"></asp:Label>
            <br /><asp:TextBox ID="TextBox_dev" autocomplete="off" TabIndex="-1" onkeydown="return false;" onkeypress="return false;" onkeyup="return false;" runat="server"></asp:TextBox><ajaxToolkit:ColorPickerExtender ID="ColorPicker_Support" runat="server" TargetControlID="TextBox_dev" SampleControlID="TextBox_dev" />
            <br />

                        &nbsp;&nbsp;&nbsp;&nbsp;</center>
                </td>
            </tr>
 </table>
                        <asp:Label ID="Label_Error" runat="server" CssClass="errorText" Visible="false"></asp:Label><br /><br />
           <center><asp:Button ID="Button_Save" runat="server" Text="Save" OnClick="Button_Save_Click" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button_Revert" runat="server" Text="Revert" OnClick="Button_Revert_Click" />
           </center>    

            <h3>Data Base Controls:</h3>
            <asp:Button ID="Button_ClearAllInstances" runat="server" Text="Delete All Subject Instances" OnClick="Button_ClearAllInstances_Click" />
        
                    
        <br />
            </center>
        <br />

    </div>
    <%-- Popup_YesNo --%>
    <asp:Panel ID="Popup_Yesno" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white"><asp:Label ID="Popup_Yesno_Label_Title" runat="server" Text="Title"></asp:Label></h2></b></center>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
            <tr>
                <td width="100%">
                    <center><asp:Label ID="Popup_Yesno_Label_Contents" runat="server"></asp:Label></center>
                    <center><asp:Button ID="Popup_Yesno_Button_Yes" runat="server" Text="Yes" OnClick="Popup_Yesno_Button_Yes_Click"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="Popup_Yesno_Button_No" runat="server" Text="No" /></center>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Popup_Yesno" TargetControlID="btnMockYESNO" CancelControlID="Popup_Yesno_Button_No" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

    <%-- Message Box Panel --%>
    <asp:Panel ID="Popup_MSG" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white"><asp:Label ID="Popup_MSG_Label_Title" runat="server" Text="Title"></asp:Label></h2></b></center>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
            <tr>
                <td width="auto">
                    <asp:Label ID="Popup_MSG_Label_contents" runat="server" Text="subject_Messagebox"></asp:Label>
                    <center><asp:Button ID="Popup_MSG_Button_Ok" runat="server" Text="Ok"/></center>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Popup_MSG" TargetControlID="btnMockMSG" CancelControlID="Popup_MSG_Button_Ok" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

    <asp:Button ID="btnMockYESNO" Text="" Style="visibility: hidden; display: none;" runat="server" />
    <asp:Button ID="btnMockMSG" Text="" Style="visibility: hidden; display: none;" runat="server" />
</asp:Content>
