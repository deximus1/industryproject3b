﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="IndustryProject3A._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

     <div class="Jumbotron">
         <br />
        <img src="Images/Banner.png" />
    </div>

   <div class="col-md-pull-11">
        <center>
        <h2>Welcome to the Didasko Online Scheduling System</h2>
        </center>
        <div class="col-md-4">
            <h2>Subject & Lecturer Management</h2>
            <p>
                Create instances of subjects, add/change lecturer information and schedule subjects.
            </p>
        </div>

        <div class="col-md-4">
            <h2>Export Calendar</h2>
            <p>
               Export your schedule in an easy to understand format.
            </p>
        </div>
    </div>

        
   

</asp:Content>
