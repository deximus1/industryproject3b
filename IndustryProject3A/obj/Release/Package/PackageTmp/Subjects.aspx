﻿<%@ Page Title="Subjects" Language="C#" MasterPageFile="~/Site.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="Subjects.aspx.cs" Inherits="IndustryProject3A.Subjects" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .modalBackground {
            background-color: black;
            filter: alpha(opacity=0.9) !important;
            opacity: 0.6 !important;
            z-index: 20;
        }

        .modalPopup {
            padding: 20px 10px 24px 10px;
            position: relative;
            width: auto;
            height: auto;
            background-color: #004A80;
            border: 1px solid black;
            top: 0px;
            left: 1px;
        }

        .errorText {
            padding: 2px 2px 2px 2px;
            position: static;
            background-color: lightcoral;
            border: 1px solid red;
        }
    </style>
    <div class="Jumbotron">
        <br />
        <img src="Images/Banner.png" />
    </div>
    
    <div class="col-md-11">
        <center>
        <h2>Subjects</h2>
        <table cellspacing="10" cellpadding="10">
            <tr>
                <td>
                    <p>
                        <asp:Panel runat="server" DefaultButton="SearchButton">
                            <asp:TextBox ID="SearchBox" autocomplete="off" runat="server"></asp:TextBox>
                            <asp:Button ID="SearchButton" runat="server" Text="Search" OnClick="SearchButton_Click" />
                        </asp:Panel>
                        <br />
                        <asp:ListBox ID="subjectList" AutoPostBack="true" runat="server" style="width: 1000px; height: 500px;"></asp:ListBox>
                    </p>
                </td>
            </tr>
        </table>
        <asp:Button ID="addSubject" runat="server" Text="Add Subject" OnClick="addSubject_Click" />
        <asp:Button ID="btnMockadd" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="editSubject" runat="server" Text="Edit Subject" OnClick="editSubject_Click" />
        <asp:Button ID="btnMockedit" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="deleteSubject" runat="server" Text="Delete Subject" OnClick="deleteSubject_Click" />
        <asp:Button ID="btnMockDelete" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="btnMockEditCancel" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="btnMockAddCancel" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="btnMockMSG" runat="server" Text="" Style="visibility: hidden; display: none;" />
        </center>
    </div>

        

    <asp:Panel ID="newSubject" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white">New Subject</h2></b></center>
        <table border="0" cellspacing="5px" cellpadding="5px" style="padding: 3px; margin: 3px; background-color: white; width: 98%;">
            <tr>
                  <td>&nbsp;&nbsp; &nbsp;</td>
                <td width="auto">
                    <br />
                    Name:
                    <asp:TextBox ID="newSubject_TextBox_Name" runat="server" autocomplete="off" Width="300px"></asp:TextBox>
                    <br />
                    Unit ID:
                    <br />
                    <asp:TextBox ID="newSubject_TextBox_UnitID" runat="server" autocomplete="off"></asp:TextBox>
                    <br />
                    <asp:Label ID="newSubject_Label_Error" runat="server" CssClass="errorText" Visible="false"></asp:Label>
                    <br>
                    </td>
                  <td>&nbsp;&nbsp; &nbsp;</td>
            </tr>
        </table>
        <br />
        <asp:Button ID="newSubject_Button_Submit" runat="server" Text="Submit" OnClick="newSubject_Button_Submit_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
       
        <asp:Button ID="newSubject_Button_Cancel" runat="server" Text="Cancel" OnClick="newSubject_Button_Cancel_Click" />
    </asp:Panel>
    <br />

    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" CancelControlID="btnMockAddCancel" PopupControlID="newSubject" TargetControlID="btnMockadd" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>


    <asp:Panel ID="editSubjectPanel" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white">Edit Subject</h2></b></center>
        <table border="0" cellspacing="1" cellpadding="1" style="background-color: white; width: 98%; height: auto">
            <tr>
                <td>&nbsp;&nbsp; &nbsp;</td>
                <td width="auto">  
                    <br />
                    Name:
                    <asp:TextBox ID="editSubject_Textbox_Name" runat="server" autocomplete="off" Width="300px"></asp:TextBox>
                    <br />
                    Unit ID:
                    <br />
                    <asp:TextBox ID="editSubject_Textbox_UnitID" runat="server" autocomplete="off"></asp:TextBox>
                    <br />
                    <asp:Label ID="editSubject_Label_Error" runat="server" CssClass="errorText" Visible="false"></asp:Label>
                    <br />
                </td>
                <td>&nbsp;&nbsp; &nbsp;</td>
            </tr>
        </table>
        <br />
        <asp:Button ID="editSubject_Button_Submit" runat="server" Text="Submit" OnClick="editSubject_Button_Submit_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="editSubject_Button_Cancel" runat="server" Text="Cancel" OnClick="editSubject_Button_Cancel_Click" />
    </asp:Panel>
    <br />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" CancelControlID="btnMockEditCancel" PopupControlID="editSubjectPanel" TargetControlID="btnMockedit" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

    <br />
    <asp:Panel ID="deleteConfirm" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white">Confirm Delete</h2></b></center>
        <table border="0" cellspacing="1" cellpadding="1" style="background-color: white; width: 98%; height: auto">
            <tr>
                 <td>&nbsp;&nbsp; &nbsp;</td>
                <td width="auto">
                    <br />
                    <center><asp:Label ID="deleteSubject_Label" runat="server"></asp:Label></center>
                    <br />
                    <center><asp:Button ID="deleteSubject_Button_Yes" runat="server" Text="Yes" OnClick="deleteSubject_Button_Yes_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="deleteSubject_Button_No" runat="server" Text="No" /></center>
                    <br />
                </td>
                 <td>&nbsp;&nbsp; &nbsp;</td>
            </tr>
        </table>
    </asp:Panel>

    <br />

    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server" CancelControlID="deleteSubject_Button_No" PopupControlID="deleteConfirm" TargetControlID="btnMockDelete" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>


      <asp:Panel ID="messageBox" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white"><asp:Label ID="messageBox_label_title" runat="server" Text="subject_Messagebox"></asp:Label></h2></b></center>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
            <tr>
                <td width="auto">
                    <center><asp:Label ID="messageBox_label_contents" runat="server" Text="subject_Messagebox"></asp:Label></center>
                    <br />
                    <center><asp:Button ID="messageBox_Button_Ok" runat="server" Text="Ok"/></center>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender4" runat="server" PopupControlID="messageBox" TargetControlID="btnMockMSG" CancelControlID="messageBox_Button_Ok" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>


</asp:Content>
