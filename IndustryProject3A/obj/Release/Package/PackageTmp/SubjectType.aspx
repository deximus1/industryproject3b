﻿<%@ Page Title="Subject Types" Language="C#" MasterPageFile="~/Site.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="SubjectType.aspx.cs" Inherits="IndustryProject3A.SubjectType" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .modalBackground {
            background-color: black;
            filter: alpha(opacity=0.9) !important;
            opacity: 0.6 !important;
            z-index: 20;
        }

        .modalPopup {
            padding: 20px 10px 24px 10px;
            position: relative;
            width: auto;
            height: auto;
            background-color: #004A80;
            border: 1px solid black;
            top: 0px;
            left: 1px;
        }

        .errorText {
            padding: 2px 2px 2px 2px;
            position: relative;
            background-color: lightcoral;
            border: 1px solid red;
        }
    </style>
    <div class="Jumbotron">
        <br />
        <img src="Images/Banner.png" />
    </div>

    <div class="col-md-11">
        <center>
        <h2>Subject Types</h2>
        <table cellspacing="10" cellpadding="10">
            <tr>
                <td>

                    <p>
                        <asp:Panel runat="server" DefaultButton="SearchButton">
                            <asp:TextBox ID="SearchBox" autocomplete="off" runat="server"></asp:TextBox>
                            <asp:Button ID="SearchButton" runat="server" Text="Search" OnClick="SearchButton_Click" />
                        </asp:Panel>
                        <br />
                        <asp:ListBox ID="subjectTypeList" AutoPostBack="true" runat="server" Height="250px" Width="500px"></asp:ListBox>
                    </p>
                </td>
            </tr>
        </table>
        <asp:Button ID="addSubjectType" runat="server" Text="Add Subject Type" OnClick="addSubjectType_Click" />
        <asp:Button ID="btnMockadd" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="editSubjectType" runat="server" Text="Edit Subject Type" OnClick="editSubjectType_Click" />
        <asp:Button ID="btnMockedit" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="deleteSubjectType" runat="server" Text="Delete Subject Type" OnClick="deleteSubjectType_Click" />
        <asp:Button ID="btnMockDelete" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="btnMockEditCancel" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="btnMockAddCancel" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="btnMockMSG" runat="server" Text="" Style="visibility: hidden; display: none;" />
        </center>
    </div>

    <asp:Panel ID="newSubjectType" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white">New Subject Type</h2></b></center>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%;">
            <tr>
                <td>&nbsp;&nbsp; &nbsp;</td>
                <center>
                <td>
                    <br />
                    Name:
                   <br />
                    <asp:TextBox ID="newSubjectType_TextBox_Name" runat="server" autocomplete="off"></asp:TextBox>
                    <br />
                    <br />
                    Load:
                   <br />
                    <asp:TextBox ID="newSubjectType_TextBox_Load" runat="server" autocomplete="off" MaxLength="4"></asp:TextBox>
                    <br />
                    <asp:Label ID="newSubjectType_Label_error" runat="server" CssClass="errorText" Visible="false"></asp:Label>
                    <br />
                    </td>
            <td>&nbsp;&nbsp; &nbsp;</td>
                </center>
                    </tr>
        </table>
        <br />
        <center>
        <asp:Button ID="newSubjectType_Button_Submit" runat="server" Text="Submit" OnClick="newSubjectType_Button_Submit_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="newSubjectType_Button_Cancel" runat="server" Text="Cancel" OnClick="newSubjectType_Button_Cancel_Click" />
    </center>
            </asp:Panel>
    <br />

    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" CancelControlID="btnMockAddCancel" PopupControlID="newSubjectType" TargetControlID="btnMockadd" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>


    <asp:Panel ID="editSubjectTypePanel" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white">&nbsp; Edit Subject Type </h2></b></center>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
            <tr>
                <td>&nbsp;&nbsp; &nbsp;</td>
                <center>
                    
                <td>
                    <br />
                    Name:
                    <br />
                    <asp:TextBox ID="editSubjectType_TextBox_Name" runat="server" autocomplete="off"></asp:TextBox>
                    <br />
                    Load:
                  
                    <br />
                    <asp:TextBox ID="editSubjectType_TextBox_Load" runat="server" autocomplete="off" MaxLength="4"></asp:TextBox>
                    <br />
                    <asp:Label ID="editSubjectType_Label_error" runat="server" CssClass="errorText" Visible="false"></asp:Label>
                <br />
                </td>
                </center>
                <td>&nbsp;&nbsp; &nbsp;</td>
            </tr>
        </table>
        <br />
        <center>
        <asp:Button ID="editSubjectType_Button_Submit" runat="server" Text="Submit" OnClick="editSubjectType_Button_Submit_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="editSubjectType_Button_Cancel" runat="server" Text="Cancel" OnClick="editSubjectType_Button_Cancel_Click" />
    </center>
            </asp:Panel>
    <br />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" CancelControlID="btnMockEditCancel" PopupControlID="editSubjectTypePanel" TargetControlID="btnMockedit" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

    <br />
    <asp:Panel ID="deleteConfirm" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white">Confirm Delete</h2></b></center>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
            <tr>
                <td width="auto">
                    <br />
                    <center><asp:Label ID="deleteSubjectType_Label" runat="server"></asp:Label></center>
                    <br />
                    <center><asp:Button ID="deleteSubjectType_Button_Yes" runat="server" Text="Yes" OnClick="deleteSubjectType_Button_Yes_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="deleteSubjectType_Button_No" runat="server" Text="No" /></center>
                <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server" CancelControlID="deleteSubjectType_Button_No" PopupControlID="deleteConfirm" TargetControlID="btnMockDelete" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

      <asp:Panel ID="messageBox" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white"><asp:Label ID="messageBox_label_title" runat="server" Text="subject_Messagebox"></asp:Label></h2></b></center>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
            <tr>
                <td width="auto">
                    <br />
                    <center><asp:Label ID="messageBox_label_contents" runat="server" Text="subject_Messagebox"></asp:Label></center>
                    <br />
                    <center><asp:Button ID="messageBox_Button_Ok" runat="server" Text="Ok"/></center>
                <br />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender4" runat="server" PopupControlID="messageBox" TargetControlID="btnMockMSG" CancelControlID="messageBox_Button_Ok" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

</asp:Content>
