﻿<%@ Page Title="Users" Language="C#" MasterPageFile="~/Site.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="IndustryProject3A.Users" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%-- Inline Style --%>
    <style>
        .modalBackground {
            background-color: black;
            filter: alpha(opacity=0.9) !important;
            opacity: 0.6 !important;
            z-index: 20;
        }

        .modalPopup {
            padding: 20px 10px 24px 10px;
            position: relative;
            width: auto;
            height: auto;
            background-color: #004A80;
            border: 1px solid black;
            top: 0px;
            left: 1px;
        }

        .errorText {
            padding: 2px 2px 2px 2px;
            position: relative;
            background-color: lightcoral;
            border: 1px solid red;
        }
    </style>

    <%-- Title Image --%>
    <div class="Jumbotron">
        <br />
        <img src="Images/Banner.png" />
    </div>

    <%-- Page Title --%>
    <center>
    <div class="col-md-11">
        
        <h2>Users</h2>
            <table cellspacing="10" cellpadding="10">
                <tr>
                    <td>
                        <%-- Page Content --%>
                        <p>
                            <asp:Panel runat="server" DefaultButton="SearchButton">
                                <asp:TextBox ID="SearchBox" autocomplete="off" runat="server"></asp:TextBox>
                                <asp:Button ID="SearchButton" runat="server" Text="Search" OnClick="SearchButton_Click" />
                            </asp:Panel>
                            <br />
                            <asp:ListBox ID="userList" AutoPostBack="true" runat="server" style="Height: 250px; Width: 300px;"></asp:ListBox>
                        </p>
                    </td>
                </tr>
            </table>
        <asp:Button ID="addUser" runat="server" Text="Add User" OnClick="addUser_Click" />
        <asp:Button ID="btnMockadd" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="editUser" runat="server" Text="Edit User" OnClick="editUser_Click" />
        <asp:Button ID="btnMockedit" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="deleteUser" runat="server" Text="Delete User" OnClick="deleteUser_Click" />
        <asp:Button ID="btnMockDelete" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="btnMockEditCancel" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="btnMockAddCancel" runat="server" Text="" Style="visibility: hidden; display: none;" />
        <asp:Button ID="btnMockMSG" runat="server" Text="" Style="visibility: hidden; display: none;" />

    </div>
    </center>


    <%-- Popups --%>
    <%-- New User --%>
    <asp:Panel ID="newUser" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white">New User</h2></b></center>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: auto;">
            <tr>
                <td>&nbsp;&nbsp; &nbsp;</td>
                <td>
                    <br />
                    Username:
                    <br />
                    <asp:TextBox ID="newUser_TextBox_Username" runat="server" autocomplete="off"></asp:TextBox>
                    <br />
                    Password:
                    <br />
                    <asp:TextBox ID="newUser_TextBox_Password" runat="server" autocomplete="off"></asp:TextBox>
                    <br />
                    Privilege:
                    <br />
                    <asp:DropDownList ID="newUser_DropDown_Privillege" runat="server" AutoPostBack="True" OnSelectedIndexChanged="newUser_DropDown_Privillege_SelectedIndexChanged">
                        <asp:ListItem>Lecturer</asp:ListItem>
                        <asp:ListItem>Manager</asp:ListItem>
                        <asp:ListItem>Administrator</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <asp:Label ID="newUser_Label_Role" runat="server">Role:</asp:Label>
                    <br />
                    <asp:RadioButtonList ID="newUser_Role" runat="server">
                        <asp:ListItem Selected="True">Full Time</asp:ListItem>
                        <asp:ListItem>Part Time</asp:ListItem>
                    </asp:RadioButtonList>
                    <br />
                    <asp:Label ID="newUser_Label_Error" runat="server" CssClass="errorText" Visible="false"></asp:Label>
                    <br />
                </td>
                <td>&nbsp;&nbsp; &nbsp;</td>
                <td style="width: auto;">
                    <br />
                    <asp:Label ID="newUser_Label_Subjects" runat="server">Subjects:</asp:Label>
                    <br />
                    <div id="newUser_Div1" style="overflow-y: scroll; width: 400px; height: 350px" runat="server">
                        <asp:CheckBoxList ID="newUser_CheckBoxList_Subjects" runat="server" Width="380px" Height="250px"></asp:CheckBoxList>
                    </div>
                    <br />
                </td>
                <td>&nbsp;&nbsp; &nbsp;</td>
            </tr>
        </table>
        <br />
        <asp:Button ID="newUser_Button_Submit" runat="server" Text="Submit" OnClick="newUser_Button_Submit_Click" CausesValidation="False" />&nbsp;&nbsp;&nbsp;&nbsp;
       
        <asp:Button ID="newUser_Button_Cancel" runat="server" Text="Cancel" OnClick="newUser_Button_Cancel_Click" />
    </asp:Panel>
    <br />

    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" CancelControlID="btnMockAddCancel" PopupControlID="newUser" TargetControlID="btnMockadd" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

    <%-- Edit User --%>
    <asp:Panel ID="editUserPanel" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white">Edit User</h2></b></center>
        <table border="0" cellspacing="5px" cellpadding="5px" style="padding: 5px; margin: 5px; background-color: white; width: auto; height: auto; border-spacing: 5px;">
            <tr>
                <br />
                <td>&nbsp;&nbsp; &nbsp;</td>
                <td>
                    <br />
                    Name:
                    <br />
                    <asp:TextBox ID="editUser_Textbox_Username" runat="server" autocomplete="off"></asp:TextBox>
                    <br />
                    Password:
                    <br />
                    <asp:TextBox ID="editUser_Textbox_Password" runat="server" autocomplete="off"></asp:TextBox>
                    <br />
                    <asp:Label ID="editUser_Label_priv" runat="server">Privillege:</asp:Label>
                    <br />
                        <asp:DropDownList ID="editUser_DropDown_Privillege" runat="server" AutoPostBack="True" OnSelectedIndexChanged="editUser_DropDown_Privillege_SelectedIndexChanged">
                            <asp:ListItem>Lecturer</asp:ListItem>
                            <asp:ListItem>Manager</asp:ListItem>
                            <asp:ListItem>Administrator</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <div id="editUser_Div2" runat="server">
                        <asp:Label ID="editUser_Label_role" runat="server">Role:</asp:Label>
                        <br />
                        <asp:RadioButtonList ID="editUser_Role" runat="server">
                            <asp:ListItem>Full Time</asp:ListItem>
                            <asp:ListItem>Part Time</asp:ListItem>
                        </asp:RadioButtonList>
                        <br />
                        <asp:Label ID="editUser_Label_anual" runat="server">Annual Leave:</asp:Label>
                        <br />
                        <asp:Label ID="editUser_Label_anualstart" runat="server">Start</asp:Label>
                        <br />
                        <asp:TextBox ID="editUser_Textbox_anualstart" runat="server" autocomplete="off"></asp:TextBox>
                        <br />
                        <asp:Label ID="editUser_Label_anualend" runat="server">End</asp:Label>
                        <br />
                        <asp:TextBox ID="editUser_Textbox_anualend" runat="server" autocomplete="off"></asp:TextBox>
                        <br />
                    </div>
                    <br />
                    <asp:Label ID="editUser_Label_Error" runat="server" CssClass="errorText" Visible="false"></asp:Label>
                    <br />
                </td>
                <td>&nbsp;&nbsp; &nbsp;</td>
                <td style="width: auto;">
                    <br />
                    <asp:Label ID="editUser_Label_Subjects" runat="server">Subjects:</asp:Label>
                    <br />
                    <div id="editUser_Div" style="overflow-y: scroll; width: 400px; height: 350px" runat="server">
                        <asp:CheckBoxList ID="editUser_CheckBoxList_Subjects" runat="server" Width="380px"></asp:CheckBoxList>
                    </div>
                    <br />
                </td>
                <td>&nbsp;&nbsp; &nbsp;</td>
            </tr>
        </table>
        <br />

        <asp:Button ID="editUser_Button_Submit" runat="server" Text="Submit" OnClick="editUserSubmit_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="editUser_Button_Cancel" runat="server" Text="Cancel" OnClick="editUserCancel_Click" />
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" CancelControlID="btnMockEditCancel" PopupControlID="editUserPanel" TargetControlID="btnMockedit" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>
    <ajaxToolkit:CalendarExtender ID="editUser_cExtender_start" runat="server" TargetControlID="editUser_Textbox_anualstart" Format="dd/MM/yyyy" />
    <ajaxToolkit:CalendarExtender ID="editUser_cExtender_end" runat="server" TargetControlID="editUser_Textbox_anualend" Format="dd/MM/yyyy" />

    <%-- Confirm Delete Panel --%>
    <asp:Panel ID="deleteConfirm" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white">Confirm Delete</h2></b></center>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
            <tr>
                <td width="100%">
                    <center><asp:Label ID="deleteUser_Label" runat="server"></asp:Label></center>
                    <br />
                    <center><asp:Button ID="deleteUser_Button_Yes" runat="server" Text="Yes" OnClick="deleteUser_Button_Yes_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="deleteUser_Button_No" runat="server" Text="No" /></center>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server" CancelControlID="deleteUser_Button_No" PopupControlID="deleteConfirm" TargetControlID="btnMockDelete" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

    <%-- Message Box Panel --%>
    <asp:Panel ID="messageBox" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white"><asp:Label ID="messageBox_label_title" runat="server" Text="subject_Messagebox"></asp:Label></h2></b></center>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
            <tr>
                <td width="auto">
                    <center><asp:Label ID="messageBox_label_contents" runat="server" Text="subject_Messagebox"></asp:Label></center>
                    <br />
                    <center><asp:Button ID="messageBox_Button_Ok" runat="server" Text="Ok"/></center>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender4" runat="server" PopupControlID="messageBox" TargetControlID="btnMockMSG" CancelControlID="messageBox_Button_Ok" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

</asp:Content>
