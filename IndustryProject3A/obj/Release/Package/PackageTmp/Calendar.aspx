﻿<%@ Page Title="Calendar" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Calendar.aspx.cs" Inherits="IndustryProject3A.WebForm5" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .modalBackground {
            background-color: black;
            filter: alpha(opacity=0.9) !important;
            opacity: 0.6 !important;
            z-index: 20;
        }

        .modalPopup {
            padding: 20px 10px 24px 10px;
            position: relative;
            width: auto;
            height: auto;
            background-color: #004A80;
            border: 1px solid black;
            top: 0px;
            left: 1px;
        }

        .modalBackdrop {
            z-index: -1;
        }

        .errorText {
            padding: 2px 2px 2px 2px;
            position: relative;
            background-color: lightcoral;
            border: 1px solid red;
        }
    </style>
    <div class="Jumbotron">
        <br />
        <img src="Images/Banner.png" />
    </div>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label_Username" Style="font-size: 26px; font-weight: bold" runat="server"></asp:Label>
    <br />
    <br />
    <asp:Button ID="btnMock" runat="server" Text="" Style="visibility: hidden; display: none;" />
    <asp:Button ID="btnMock2" runat="server" Text="" Style="visibility: hidden; display: none;" />
    <asp:Button ID="Button_Export" runat="server" Text="Export" />
     <h2><b><asp:Label id="Label_error" runat="server" text=""></asp:Label></b></h2>
    <asp:UpdatePanel ID="UpdatePanel" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server">
        <ContentTemplate>
            <center>
    <asp:Button ID="Button_Year1_Down" runat="server" Text="←" CssClass="viewButton" OnClick="Button_Year1_Down_Click" /><asp:Button ID="Button_Year1_Up" runat="server" Text="→" CssClass="viewButton" OnClick="Button_Year1_Up_Click" />&nbsp;<asp:Label ID="Label_Year" runat="server"></asp:Label> &nbsp;<asp:Button ID="Button_Year2_Down" runat="server" Text="←" OnClick="Button_Year2_Down_Click" /><asp:Button ID="Button_Year2_Up" runat="server" Text="→" OnClick="Button_Year2_Up_Click" />
    <div id="scrollDiv" style="overflow-y: auto; width: 80%; height: 80%">
        <asp:HiddenField ID="SelectedGridCellIndex" runat="server" Value="-1" />
        <asp:GridView ID="GridView_Instances" AutoGenerateColumns="true" ShowHeaderWhenEmpty="true" runat="server" EmptyDataText="No Records found!" RowStyle-BackColor="white" RowStyle-Wrap="False" OnRowCreated="GridView_Instances_RowCreated" OnRowDataBound="GridView_Instances_RowDataBound" OnDataBound="GridView_Instances_DataBound" OnSelectedIndexChanged="GridView_Instances_SelectedIndexChanged">
            <RowStyle BackColor="White" Wrap="false" />
        </asp:GridView>
    </div>
    </center>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="UpdatePanel_Popup" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server">
                <ContentTemplate>

    <asp:Panel ID="iViewer_Panel" runat="server" CssClass="modalPopup">
        <center><b><h2 style="color:white"><asp:Label ID="iViewer_Label_Title" runat="server" Text="Title"></asp:Label></h2></b>
        <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
            <tr>
                <td>
                    <asp:Label ID="iViewer_Label_Students" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="iViewer_Label_Desc" runat="server"></asp:Label>
                    <br />
                    <asp:ListBox ID="iViewer_Listbox" runat="server"></asp:ListBox>
                    <br />
                    <center><asp:Button ID="iViewer_Button_Ok" runat="server" Text="Ok" OnClick="iViewer_Button_Ok_Click"/></center>
                </td>
            </tr>
        </table>
       </center>
    </asp:Panel>
    </center>
        </ContentTemplate>
        </asp:UpdatePanel>
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="UpdatePanel_Popup" TargetControlID="btnMock" CancelControlID="btnMock2" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>


    <script type="text/javascript"> 
        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('scrollDiv').scrollLeft;
            yPos = $get('scrollDiv').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('scrollDiv').scrollLeft = xPos;
            $get('scrollDiv').scrollTop = yPos;
        }
    </script>

</asp:Content>
