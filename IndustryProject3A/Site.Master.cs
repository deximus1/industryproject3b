﻿using System;
using System.Configuration;
using System.Web.UI;
using IndustryProject3A.Classes;

// @Author Luke Dunning

namespace IndustryProject3A
{
    public partial class SiteMaster : MasterPage
    {

        protected void MainContent_Init(object sender, EventArgs e)
        {
            try
            {
                SQLHandler sql = new SQLHandler();
                //Log-on/Privellege Check/Modifiers
                if (String.IsNullOrEmpty((string)Session["Privilege"]))//No Session detected
                {
                    Response.Redirect("Login.aspx");
                }
                else if (Session["Privilege"].Equals("2"))//Admin Session
                {
                    link_instance.Visible = true;
                    link_sType.Visible = true;
                    link_subjects.Visible = true;
                    link_users.Visible = true;
                    link_admin.Visible = true;
                }
                else if (Session["Privilege"].Equals("1"))//Manager Session
                {
                    //link_instance.Visible = true;
                }
                else if (Session["Privilege"].Equals("0"))//User Session
                {
                    //link_calendar.Visible = true;
                }
                Label_Username.Text = (string)Session["Username"];

                if (!String.IsNullOrEmpty((string)Session["UserID"]) && !sql.userExists(Convert.ToInt32(Session["UserID"]))) //Check to see if account has been deleted
                {
                    Response.Write("<script>alert('Error code: 99');</script>");
                    Logout();
                }


                //Page Verification Checks
                string pageName = (string)((string)(this.MainContent.Page.GetType().FullName).Split('.').GetValue(1)).Split('_').GetValue(0); //resolve page name from child
                int pagePrivLevel = Convert.ToInt32(ConfigurationManager.AppSettings[pageName]); //check priv level in web.config with pagename
                if (pagePrivLevel > 0 && pagePrivLevel > Convert.ToInt32(Session["Privilege"])) //If page requires higher priv and priv is higher than user priv
                {
                    Response.Redirect("Default.aspx"); //Access Denied
                }
            }
            catch (Exception ex)
            {

            }

        }

        private void Logout()
        {
            Session.Clear();
            Response.Redirect("Login.aspx");
        }

        protected void LinkButton_Logout_Click(object sender, EventArgs e)
        {
            Logout();
        }
    }
}