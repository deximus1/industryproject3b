﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public class InstanceCounter //Counts all instances loads for use by the instance manager and calendar page
    {
        public List<Instance> instances { get; set; }
        public InstanceCounter()
        {
            instances = new List<Instance>();
        }

        public float getTotal(List<Subject_Type> sTypeStore)
        {
            float total = 0.0f;
            foreach (Instance i in instances)
            {
                float temp = countInstance(i,sTypeStore);
                if (temp > (float)0.0f)
                {
                    total += temp;
                }
            }
            return total;
        }

        public float countInstance(Instance i, List<Subject_Type> sTypeStore)
        {
            int index = Lists.getObjectIndex(sTypeStore, i.SubjectTypeID);
            float temp = 0.0f;
            temp += sTypeStore[index].Load;

            if (i.isPrimary) //Only adds student weight count to Primary lecturer
            {
                int students = i.students;
                if (students > 120) //Student load weightings
                {
                    temp += (float)0.6;
                }
                else if (students > 90)
                {
                    temp += (float)0.45;
                }
                else if (students > 60)
                {
                    temp += (float)0.30;
                }
                else if (students > 30)
                {
                    temp += (float)0.15;
                }
                temp -= (i.SupportAmount * sTypeStore[1].Load);
            }
            return temp;
        }
    }
}