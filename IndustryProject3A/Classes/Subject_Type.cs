﻿namespace IndustryProject3A.Classes
{
    // @Author Luke Dunning

    public class Subject_Type // Subject Types used by the instance manager
    {
        public int SubjectTypeID { get; set; }
        public string Name { get; set; }
        public float Load { get; set; }

        public Subject_Type(int SubjectTypeID, string Name, float Load)
        {
            this.SubjectTypeID = SubjectTypeID;
            this.Name = Name;
            this.Load = Load;
        }

        public override string ToString() // method used by the two search methods in the List class
        {
            return SubjectTypeID +"|"+ Name;
        }
    }
}