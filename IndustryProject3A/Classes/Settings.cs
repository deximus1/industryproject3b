﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public class Settings //Settings for the administrator
    {
        private List<string> orignalVars; //unpacked vars
        public List<string> newVars;  //packed vars ready for comparison to determine what has changed

        public float loadMin, loadMax; //miniumum and maximum load for the instance manager
        public Color c_NoLecturer, c_NoPrimary, c_Ok, c_hasAnnual, c_underUte, c_goodUte, c_overUte, c_teachLead,c_teachSupport, c_dev; //converted colours


        public Settings(List<string> vars)
        {
            this.orignalVars = vars;
            newVars = new List<string>();
            UnPack();
        }

        private void UnPack() //unpack vars into convert vars
        {
            try
            {
                loadMin = float.Parse(orignalVars[0], CultureInfo.InvariantCulture.NumberFormat);
                loadMax = float.Parse(orignalVars[1], CultureInfo.InvariantCulture.NumberFormat);
                c_NoLecturer = ConvertHexToColour(orignalVars[2]);
                c_NoPrimary = ConvertHexToColour(orignalVars[3]);
                c_Ok = ConvertHexToColour(orignalVars[4]);
                c_hasAnnual = ConvertHexToColour(orignalVars[5]);
                c_underUte = ConvertHexToColour(orignalVars[6]);
                c_goodUte = ConvertHexToColour(orignalVars[7]);
                c_overUte = ConvertHexToColour(orignalVars[8]);
                c_teachLead = ConvertHexToColour(orignalVars[9]);
                c_teachSupport = ConvertHexToColour(orignalVars[10]);
                c_dev = ConvertHexToColour(orignalVars[11]);
            }
            catch (Exception ex)
            {

            }
        }

        public void Pack() //pack vars for use in cross comparison
        {
            newVars.Clear();
            newVars.Add(loadMin.ToString());
            newVars.Add(loadMax.ToString());
            newVars.Add(ConvertColourtoString(c_NoLecturer));
            newVars.Add(ConvertColourtoString(c_NoPrimary));
            newVars.Add(ConvertColourtoString(c_Ok));
            newVars.Add(ConvertColourtoString(c_hasAnnual));
            newVars.Add(ConvertColourtoString(c_underUte));
            newVars.Add(ConvertColourtoString(c_goodUte));
            newVars.Add(ConvertColourtoString(c_overUte));
            newVars.Add(ConvertColourtoString(c_teachLead));
            newVars.Add(ConvertColourtoString(c_teachSupport));
            newVars.Add(ConvertColourtoString(c_dev));
        }

        public bool HasChanged(int index) //checks to see if the var has changed. If it has - save it to the DB
        {
            return (orignalVars[index] != newVars[index]);
        }

        public Color ConvertHexToColour(string ColourString) //Converts a hex string to a color
        {
            return System.Drawing.ColorTranslator.FromHtml("#"+ColourString); ;
        }
        public String ConvertColourtoString(Color color) //converts a color to hex string. Removed the # at start of string becase ajaxcolourpicker didnt like it.
        {
            return System.Drawing.ColorTranslator.ToHtml(color).TrimStart('#');
        }


    }
}