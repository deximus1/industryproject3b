﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public static class States //states list used by the state machine. (Should of used an enum)
    {

        public const byte STANDBY = 0;
        public const byte ADDING = 1;
        public const byte EDITING = 2;
        public const byte DELETING = 3;
    }
}