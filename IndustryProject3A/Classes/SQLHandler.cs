﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public class SQLHandler //Holds all SQL related commands (Should of used a singleton)
    {
        public SQLHandler() //Called whenever the SQLHandler is instanced. Checks to see if the server is run locally or on the server. If locally it sets a dynamic connection string dependant on the servers local directory. If not it uses a manually entered connection string defined bellow.
        {
            if (String.IsNullOrEmpty((string)HttpContext.Current.Session["conString"]))//Check to see if connectionstring has been modified to work with local machine
            {
                bool local = false;
                if (local)
                {
                    string s = HttpContext.Current.Server.MapPath(@"\App_Data\DB.mdf"); //Get DB Directory
                    HttpContext.Current.Session["conString"] = ConfigurationManager.ConnectionStrings["DB"].ConnectionString.Replace("[DYNDIR]", s); //Add DB dir to conString and save to session
                }
                else
                {
                    HttpContext.Current.Session["conString"] = "Data Source=SQL5097.site4now.net;Initial Catalog=DB_A6C626_DB;User Id=DB_A6C626_DB_admin;Password=Password123";//Manual server connection string
                }
            }
        }

        public String getCON()//gets the connection string
        {
            return HttpContext.Current.Session["conString"].ToString();
        }

        public bool userExists(int id)//checks if a user exists based on ID
        {
            try
            {
                SqlConnection sqlCON = new SqlConnection(getCON());
                if (sqlCON.State == System.Data.ConnectionState.Closed)
                {
                    sqlCON.Open();
                }
                SqlCommand com = new SqlCommand("select * FROM Users WHERE Id='" + id.ToString() + "'", sqlCON);
                SqlDataReader sqlDR = com.ExecuteReader();
                if (sqlDR.HasRows)
                {
                    while (sqlDR.Read())
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool tryLogin(string Username, string Password)//Login method returns true or false and sets the session vars if true
        {
            try
            {
                SqlConnection sqlCON = new SqlConnection(getCON());
                if (sqlCON.State == System.Data.ConnectionState.Closed)
                {
                    sqlCON.Open();
                }
                SqlCommand com = new SqlCommand("select * FROM Users WHERE UPPER(Username)='" + Username.ToUpper() + "' AND UPPER(Password)='" + Password.ToUpper() + "' ", sqlCON);
                SqlDataReader sqlDR = com.ExecuteReader();
                if (sqlDR.HasRows)
                {
                    while (sqlDR.Read())
                    {
                        HttpContext.Current.Session["UserID"] = sqlDR.GetValue(0).ToString();
                        HttpContext.Current.Session["Username"] = sqlDR.GetValue(1).ToString();
                        HttpContext.Current.Session["Privilege"] = sqlDR.GetValue(3).ToString();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public Boolean isValueTaken(string TableName, string ColumnName, string Value) //checks if a value is taken within a specific table column
        {
            try
            {
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    if (sqlCON.State == System.Data.ConnectionState.Closed)
                    {
                        sqlCON.Open();
                    }
                    SqlCommand sqlCOM = new SqlCommand("select * FROM " + TableName + " WHERE UPPER(" + ColumnName + ")='" + Value.ToUpper() + "'", sqlCON);
                    SqlDataReader sqlDR = sqlCOM.ExecuteReader();
                    if (sqlDR.HasRows)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public int getMaxID(string TableName)//gets the max id used in a table for use in new record creation
        {
            int maxid = 0;
            try
            {
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    if (sqlCON.State == System.Data.ConnectionState.Closed)
                    {
                        sqlCON.Open();
                    }
                    SqlCommand sqlCOM = new SqlCommand("select max(id) from " + TableName, sqlCON);
                    SqlDataReader sqlDR = sqlCOM.ExecuteReader();
                    if (sqlDR.HasRows)
                    {
                        while (sqlDR.Read())
                        {
                            maxid = !sqlDR.GetValue(0).ToString().Equals("") ? Convert.ToInt32(sqlDR.GetValue(0).ToString()) : 0;
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return maxid;
        }

        public void deleteRow(string TableName, int Id)//deltes row from chosen table
        {
            try
            {
                string com = "DELETE from " + TableName + " WHERE Id = '" + Id.ToString() + "'";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void deleteAllRow(string TableName, string colName, int Id) //deletes rows with a certain id and column
        {
            try
            {
                string com = "DELETE from " + TableName + " WHERE " + colName + " = '" + Id.ToString() + "'";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        //SUBJECTS

        public List<Subject> getSubjects() //gets all subjects from DB and stores them in a list
        {
            List<Subject> tempSubjectList = new List<Subject>();
            SqlConnection sqlCON = new SqlConnection(getCON());
            try
            {
                if (sqlCON.State == System.Data.ConnectionState.Closed)
                {
                    sqlCON.Open();
                }
                SqlCommand com = new SqlCommand("select * FROM subjects", sqlCON);
                SqlDataReader sqlDR = com.ExecuteReader();
                if (sqlDR.HasRows)
                {
                    while (sqlDR.Read())
                    {
                        Subject temp = new Subject(Convert.ToInt32(sqlDR.GetValue(0)), sqlDR.GetValue(1).ToString(), sqlDR.GetValue(2).ToString());
                        tempSubjectList.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return tempSubjectList;
        }



        public void addSubject(int MaxId, string Name, string UnitID) //Add subject to DB
        {
            try
            {
                string com = "INSERT INTO subjects (Id, Name, UnitID) VALUES (@Id,@Name, @UnitID)";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", MaxId + 1);
                        cmd.Parameters.AddWithValue("@Name", Name);
                        cmd.Parameters.AddWithValue("@UnitID", UnitID);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void editSubject(int Id, string Name, string UnitID) //edit subject from DB
        {
            try
            {
                string com = "UPDATE subjects SET Name = @Name, UnitID = @UnitID Where Id = @Id";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@Name", Name);
                        cmd.Parameters.AddWithValue("@UnitID", UnitID);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        //SUBJECTTYPES

        public List<Subject_Type> getSubjectTypes() //get all Subject types and store in List
        {
            List<Subject_Type> tempSubjectTypeList = new List<Subject_Type>();
            try
            {
                SqlConnection sqlCON = new SqlConnection(getCON());
                if (sqlCON.State == System.Data.ConnectionState.Closed)
                {
                    sqlCON.Open();
                }
                SqlCommand com = new SqlCommand("select * FROM subjecttypes", sqlCON);
                SqlDataReader sqlDR = com.ExecuteReader();
                if (sqlDR.HasRows)
                {
                    while (sqlDR.Read())
                    {
                        Subject_Type temp = new Subject_Type(Convert.ToInt32(sqlDR.GetValue(0)), sqlDR.GetValue(1).ToString(), float.Parse(sqlDR.GetValue(2).ToString(), CultureInfo.InvariantCulture.NumberFormat));
                        tempSubjectTypeList.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return tempSubjectTypeList;
        }

        public void addSubjectType(int MaxId, string Name, float Load) //add subject type to db
        {
            try
            {
                string com = "INSERT INTO subjecttypes (Id, Name, Load) VALUES (@Id,@Name, @Load)";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", MaxId + 1);
                        cmd.Parameters.AddWithValue("@Name", Name);
                        cmd.Parameters.AddWithValue("@Load", Load);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void editSubjectType(int Id, string Name, float Load) //edit subject type in db
        {
            try
            {
                string com = "UPDATE subjecttypes SET Name = @Name, Load = @Load Where Id = @Id";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@Name", Name);
                        cmd.Parameters.AddWithValue("@Load", Load);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


        //USERS

        public List<User> getUsers() //get all users and store in List
        {
            List<User> tempUserList = new List<User>();
            SqlConnection sqlCON = new SqlConnection(getCON());
            try
            {
                if (sqlCON.State == System.Data.ConnectionState.Closed)
                {
                    sqlCON.Open();
                }
                SqlCommand com = new SqlCommand("select * FROM users", sqlCON);
                SqlDataReader sqlDR = com.ExecuteReader();
                if (sqlDR.HasRows)
                {
                    while (sqlDR.Read())
                    {
                        User temp = new User(Convert.ToInt32(sqlDR.GetValue(0)), sqlDR.GetValue(1).ToString(), sqlDR.GetValue(2).ToString(), Convert.ToInt32(sqlDR.GetValue(3)), sqlDR.GetValue(4).ToString(), Convert.ToDouble(sqlDR.GetValue(5)), sqlDR.GetValue(6).ToString(), sqlDR.GetValue(7).ToString(), sqlDR.GetValue(8).ToString(), sqlDR.GetByte(9));
                        tempUserList.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return tempUserList;
        }

        public void addUser(int maxid, string userName, string passWord, int priv, string ableSubjectsCSV, float load) //add user to db
        {
            try
            {
                string com = "INSERT INTO users (Id, Username, Password, Privilege, AbleSubjects, Load, AnualLeave, Sort, yearSelect) VALUES (@Id,@Username, @Password, @Privillege, @AbleSubjects, @Load, '', 'ASC', 0)";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", maxid + 1);
                        cmd.Parameters.AddWithValue("@Username", userName);
                        cmd.Parameters.AddWithValue("@Password", passWord);
                        cmd.Parameters.AddWithValue("@Privillege", priv);
                        cmd.Parameters.AddWithValue("@AbleSubjects", ableSubjectsCSV);
                        cmd.Parameters.AddWithValue("@Load", load);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void editUser(int Id, string userName, string passWord, int priv, string ableSubjectsCSV, float load, string anualleave) //edit user in db
        {
            try
            {
                string com = "UPDATE users SET Username = @Username, Password = @Password, Privilege = @Privillege, AbleSubjects = @AbleSubjects, Load = @Load, AnualLeave = @AnualLeave Where Id = @Id";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@Username", userName);
                        cmd.Parameters.AddWithValue("@Password", passWord);
                        cmd.Parameters.AddWithValue("@Privillege", priv);
                        cmd.Parameters.AddWithValue("@AbleSubjects", priv == 0 ? ableSubjectsCSV : "");
                        cmd.Parameters.AddWithValue("@Load", load);
                        cmd.Parameters.AddWithValue("@AnualLeave", priv == 0 ? anualleave : "");
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void editUserAbleSubjects(int Id, string ableSubjectsCSV) //edit users able subjects list in db
        {
            try
            {
                string com = "UPDATE users SET AbleSubjects = @AbleSubjects Where Id = @Id";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@AbleSubjects", ableSubjectsCSV);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void editUserSort(int Id, string sort) //edit users sort criteria
        {
            try
            {
                string com = "UPDATE users SET Sort = @Sort Where Id = @Id";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@Sort", sort);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void editUserYearSelect(int Id, string yearSelect) //edit users selected year range in instance manager
        {
            try
            {
                string com = "UPDATE users SET yearSelect = @yearSelect Where Id = @Id";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@yearSelect", yearSelect);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void editUserMode(int Id, string Mode) //edit users Mode ~ unsed (cant remember why i made tis)
        {
            try
            {
                string com = "UPDATE users SET Mode = @Mode Where Id = @Id";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@Mode", Mode);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        //SUBJECT_INSTANCES

        public List<Subject_Instance> getSubjectInstances() //get all subject instances from db and store in a list
        {
            List<Subject_Instance> tempSubjectInstanceList = new List<Subject_Instance>();
            try
            {
                SqlConnection sqlCON = new SqlConnection(getCON());
                if (sqlCON.State == System.Data.ConnectionState.Closed)
                {
                    sqlCON.Open();
                }
                SqlCommand com = new SqlCommand("select * FROM subjectinstances", sqlCON);
                SqlDataReader sqlDR = com.ExecuteReader();
                if (sqlDR.HasRows)
                {
                    while (sqlDR.Read())
                    {
                        bool mode = sqlDR.GetBoolean(1);
                        string tempS = sqlDR.GetValue(2).ToString();
                        Subject_Instance temp = new Subject_Instance(Convert.ToInt32(sqlDR.GetValue(0)), mode, sqlDR.GetString(2), sqlDR.GetString(3), Convert.ToInt32(sqlDR.GetValue(4)), Convert.ToInt16(sqlDR.GetValue(5)));
                        tempSubjectInstanceList.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return tempSubjectInstanceList;
        }

        public void addInstance(int maxid, bool mode, string Lecturers, string Date, int subjectID, short students) //add subject instance to DB
        {
            try
            {
                string com = "INSERT INTO subjectinstances (Id, mode, Lecturers, Date, subjectID, students) VALUES (@Id,@mode, @Lecturers, @Date, @subjectID, @students)";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", maxid + 1);
                        cmd.Parameters.AddWithValue("@mode", mode);
                        cmd.Parameters.AddWithValue("@Lecturers", Lecturers);
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@subjectID", subjectID);
                        cmd.Parameters.AddWithValue("@students", students);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void editInstance(int Id, bool mode, string Lecturers, short students) //edit subject instance in db
        {
            try
            {
                string com = "UPDATE subjectinstances SET mode = @mode, Lecturers = @Lecturers, students = @students Where Id = @Id";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@mode", mode);
                        cmd.Parameters.AddWithValue("@Lecturers", Lecturers);
                        cmd.Parameters.AddWithValue("@students", students);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


        public void editInstanceLecturers(int Id, string Lecturers) //edit subject instance lecturers in db
        {
            try
            {
                string com = "UPDATE subjectinstances SET Lecturers = @Lecturers Where Id = @Id";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@Lecturers", Lecturers);
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void deleteAllSubjectInstances() //delete all subject isntances from db
        {
            try
            {
                string com = "DELETE from subjectinstances Where Id >= 0";
                using (SqlConnection sqlCON = new SqlConnection(getCON()))
                {
                    using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                    {
                        sqlCON.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


        //SETTINGS

        public Settings getSettings() //get settings from db
        {
            List<string> vars = new List<string>();
            try
            {
                SqlConnection sqlCON = new SqlConnection(getCON());
                if (sqlCON.State == System.Data.ConnectionState.Closed)
                {
                    sqlCON.Open();
                }
                SqlCommand com = new SqlCommand("select * FROM settings", sqlCON);
                SqlDataReader sqlDR = com.ExecuteReader();
                if (sqlDR.HasRows)
                {
                    while (sqlDR.Read())
                    {
                        vars.Add(sqlDR.GetString(1));
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return new Settings(vars);
        }

        public void saveSettings(Settings settings) //save settings to db depending on what setting has changed. The Settings class can easily be changed to add more setttings. For more info check the Settings class
        {
            try
            {
                settings.Pack();
                for (int i = 0; i < settings.newVars.Count; i++)
                {
                    if (settings.HasChanged(i))
                    {
                        string com = "UPDATE settings SET value = @value Where Id = @Id";
                        using (SqlConnection sqlCON = new SqlConnection(getCON()))
                        {
                            using (SqlCommand cmd = new SqlCommand(com, sqlCON))
                            {
                                cmd.Parameters.AddWithValue("@Id", i + 1);
                                cmd.Parameters.AddWithValue("@value", settings.newVars[i]);
                                sqlCON.Open();
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}