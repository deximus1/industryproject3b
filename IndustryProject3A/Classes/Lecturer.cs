﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public class Lecturer //Used by instance counter
    {
        public int userID { get; set; }
        public int subjectTypeID { get; set; }

        public Lecturer(int userID, int subjectTypeID)
        {
            this.userID = userID;
            this.subjectTypeID = subjectTypeID;
        }

    }
}