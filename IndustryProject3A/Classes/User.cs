﻿using System;
using System.Collections.Generic;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public class User //User class
    {
        public int UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Privillege { get; set; }
        public string AbleSubjects { get; set; }
        public List<int> AbleSubjectList { get; set; }
        public double Load { get; set; }
        public string AnualLeave { get; set; }
        public DateTime AnualStart { get; set; }
        public DateTime AnualEnd { get; set; }
        public string yearSelect { get; set; }
        public short lYear { get; set; }
        public short hYear { get; set; }
        public string Sort { get; set; }
        public byte mode { get; set; }

        public User(int UserID, string Username, string Password, int Privillege, string AbleSubjects, double Load, string AnualLeave, string yearSelect, string Sort, byte mode)
        {
            this.UserID = UserID;
            this.Username = Username;
            this.Password = Password;
            this.Privillege = Privillege;
            this.Load = Load;
            this.AnualLeave = AnualLeave;
            this.AbleSubjects = AbleSubjects;
            this.AbleSubjectList = new List<int>();
            this.yearSelect = yearSelect;
            this.Sort = Sort;
            this.mode = mode;

            if (hasAbleSubjects())
            {
                convertSubjectList();
            }
            if (hasAnualLeave())
            {
                convertStringtoDate();
            }
            if (hasYearSelect())
            {
                convertYearSelect();
            }
        }

        public void convertYearSelect()
        {
            string[] temp = yearSelect.Split((char)',');
            if (temp.Length == 2)
            {
                lYear = Convert.ToInt16(temp[0]);
                hYear = Convert.ToInt16(temp[1]);
            }
        }

        public bool hasYearSelect()
        {
            return (yearSelect.Length > 0);
        }

        public bool hasAbleSubjects()
        {
            return (AbleSubjects.Length > 0);
        }

        public bool hasAnualLeave()
        {
            return (AnualLeave.Length > 0);
        }

        public void convertSubjectList()
        {
            if (AbleSubjectList.Count > 0)
            {
                AbleSubjectList.Clear();
            }
            string[] temp = AbleSubjects.Split((char)',');
            if (temp.Length > 0)
            {
                foreach (String s in temp)
                {
                    AbleSubjectList.Add(Convert.ToInt32(s));
                }
            }
        }

        public void convertConvertSubjectString()
        {
            AbleSubjects = "";
            foreach (int i in AbleSubjectList)
            {
                if (AbleSubjects.Length > 0)
                {
                    AbleSubjects += ",";
                }
                AbleSubjects += i.ToString();
            }
        }
        
        public void convertStringtoDate()
        {
            string[] temp = AnualLeave.Split((char)',');
            AnualStart = DateTime.ParseExact(temp[0], "dd/MM/yyyy", null);
            AnualEnd = DateTime.ParseExact(temp[1], "dd/MM/yyyy", null);
        }


        public string getPrivillegeName()
        {
            return Privillege == 2 ? "Administrator" : Privillege == 1 ? "Manager" : "User";
        }

        public override string ToString()
        {
            return UserID+"|"+Username;
        }

        public bool isAvail(DateTime monthStart) //Anual leave conflic checker
        {
            if (hasAnualLeave())
            {
                DateTime monthEnd = monthStart.AddMonths(2);
                if ((monthStart.Ticks >= AnualStart.Ticks && monthStart.Ticks <= AnualEnd.Ticks) || (monthEnd.Ticks >= AnualStart.Ticks && monthEnd.Ticks <= AnualEnd.Ticks))
                {
                    return false;
                }
            }
            return true;
        }

    }
}