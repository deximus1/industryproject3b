﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public class Instance //Very simular to Subject_Instance Class. Used in the InstanceSorter to store session information to be used by the instance manager
    {
        public int SubjectInstanceID { get; set; }
        public int SubjectTypeID { get; set; }
        public int students { get; set; }
        public string StartDate { get; set; }
        
        public bool isPrimary { get; set; }

        public byte SupportAmount { get; set; }

        public Instance(int SubjectInstanceID, int SubjectTypeID, int students, string StartDate ,bool isPrimary, byte SupportAmount)
        {
            this.SubjectInstanceID = SubjectInstanceID;
            this.SubjectTypeID = SubjectTypeID;
            this.students = students;
            this.StartDate = StartDate;
            this.isPrimary = isPrimary;
            this.SupportAmount = SupportAmount;
        }
    }
}