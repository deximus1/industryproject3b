﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public class Subject_Instance //Subject instance class and vars
    {
        public int SubjectInstanceID { get; set; }
        public bool mode { get; set; } // mode teach or dev
        public List<Lecturer> lecturers { get; set; } //Converted lecturers
        public string lecturersString { get; set; } // lecturers CSV (wont be doing this next time)
        public byte Month { get; set; }
        public short Year { get; set; }
        public int SubjectID { get; set; }
        public short Students { get; set; }

        private int primaryID = -1; // primary lecturer ID

        public Subject_Instance(int SubjectInstanceID, bool mode, string lecturersString, String Date, int SubjectID, short Students)
        {
            this.SubjectInstanceID = SubjectInstanceID;
            this.mode = mode;
            this.lecturersString = lecturersString;
            this.SubjectID = SubjectID;
            this.Students = Students;
            lecturers = new List<Lecturer>();
            String[] date = Date.Split('/');
            convertLecturerString();
            setDate(Convert.ToByte(date[0]), Convert.ToInt16(date[1]));
        }

        public void convertLecturerString() //pack lecturers CSV
        {
            lecturers.Clear();
            if (!lecturersString.Equals(""))
            {
                List<string> users = lecturersString.Split(',').ToList();
                foreach (String s in users)
                {
                    List<string> tokens = s.Split('|').ToList();
                    Lecturer tempLecturer = new Lecturer(Convert.ToInt32(tokens[0]), Convert.ToInt32(tokens[1]));
                    lecturers.Add(tempLecturer);
                }
            }
        }

        public void setDate(byte Month, short Year)
        {
            this.Year = Year;
            this.Month = Month;
        }

        public bool hasPrimary() //used by the instance manager to determine if there is no primary lecturer in the instance
        {
            foreach (Lecturer l in lecturers)
            {
                if (l.subjectTypeID == 0)
                {
                    primaryID = l.userID;
                    return true;
                }
            }
            return false;
        }

        public bool containtsLecturer(int userID) //used by the instance manager to determine if there is a certain lecturer in the instance
        {
            foreach (Lecturer l in lecturers)
            {
                if (l.userID == userID)
                {
                    return true;
                }
            }
            return false;
        }

        public int getPrimaryUserID() //used by the instance manager to return the primary lecturer ID
        {
            if (primaryID == -1)
            {
                hasPrimary();
            }
            return primaryID;
        }

        public string buildDateString() //pack date into string
        {
            return Month.ToString("00") + "/" + Year.ToString();
        }

        public bool isDeveloping()
        {
            return !mode;
        }

        public bool isTeaching()
        {
            return mode;
        }

        public override string ToString()
        {
            return SubjectInstanceID+"|"+SubjectID;
        }

    }
}