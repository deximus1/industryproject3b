﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public static class Lists //Generics used by various processes around the codebase
    {

        public static void loadListBox<T>(List<T> list, ListBox listbox) //Loads any list into listbox
        {
            listbox.Items.Clear();
            foreach (object ob in list)
            {
                listbox.Items.Add(new ListItem(ob.ToString().Split('|')[1]));
            }
        }

        public static void loadCheckBoxList<T>(List<T> list, CheckBoxList checkboxlist) //loads any list into checkboxlist
        {
            checkboxlist.Items.Clear();
            foreach (object ob in list)
            {
                checkboxlist.Items.Add(new ListItem(ob.ToString().Split('|')[1]));
            }
        }

        public static List<object> getSearchList<T>(List<T> list, string value) //search any list and return any matches
        {
            List<object> temp = new List<object>();
            foreach (object ob in list)
            {
                if (ob.ToString().ToLower().Contains(value.Trim().ToLower()))
                {
                    temp.Add(ob);
                }
            }
            return temp;
        }


        public static int getObjectIndex<T>(List<T> list, string value) //search through any list to find the index of matched string
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].ToString().Split('|')[1].Equals(value))
                {
                    return i;
                }
            }
            return -1;
        }

        public static int getObjectIndex<T>(List<T> list, int value) //search through any list to find the index of matched index
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (Convert.ToInt32(list[i].ToString().Split('|')[0]) == value)
                {
                    return i;
                }
            }
            return -1;
        }

        public static string addErrorText(string errorString, string error) //compile a list of errors to be used by the asp.net label
        {
            return errorString += errorString.Length > 0 ? "<br />" + error : error;
        }
    }
}