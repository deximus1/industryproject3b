﻿namespace IndustryProject3A.Classes
{

    // @Author Luke Dunning
    public class Subject // Subject class used by the site to name and give unit ids to subjects
    {
        public int SubjectID { get; set; }
        public string Name { get; set; }
        public string UnitID { get; set; }

        public Subject(int SubjectID, string Name, string UnitID)
        {
            this.SubjectID = SubjectID;
            this.Name = Name;
            this.UnitID = UnitID;
        }

        public override string ToString() // Used by the two search features within the Lists class
        {
            return SubjectID+"|"+Name;
        }
    }
}