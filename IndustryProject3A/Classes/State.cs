﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public class State //very simple state machine
    {
        public byte STATE { get; set; }

        public State()
        {

        }

        public void Set(byte STATE)
        {
            this.STATE = STATE;
            HttpContext.Current.Session["State"] = this; //sets state class to the session
        }

    }
}