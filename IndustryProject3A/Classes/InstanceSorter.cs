﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// @Author Luke Dunning

namespace IndustryProject3A.Classes
{
    public class InstanceSorter //Sorts all instances from DB in dictionaries.
    {
        public List<Subject_Instance> sInstanceStore { get; set; }
        public Dictionary<short, Dictionary<int, Dictionary<byte, Subject_Instance>>> teachDictionary { get; set; } //Teaching instances (Year, SubjectID, Month) returns Instance
        public Dictionary<short, Dictionary<int, Dictionary<byte, Subject_Instance>>> devDictionary { get; set; } //Developing instance (Year, SubjectID, Month) returns Instance
        public Dictionary<short, Dictionary<int, Dictionary<byte, InstanceCounter>>> lecDictionary { get; set; } //Loads Dictionary (Year,UserID, Month ) returns InstanceCounter
        public short teach_lYear { get; set; } //Lowest Year in teach dictionary
        public short teach_hYear { get; set; } //Highest Year in teach dictionary
        public short dev_lYear { get; set; } //Lowest Year in dev dictionary
        public short dev_hYear { get; set; } //Highest year in dev dictionary

        public InstanceSorter(List<Subject_Instance> sInstanceStore)
        {
            this.sInstanceStore = sInstanceStore; //Object with all instances
            teachDictionary = new Dictionary<short, Dictionary<int, Dictionary<byte, Subject_Instance>>>();
            devDictionary = new Dictionary<short, Dictionary<int, Dictionary<byte, Subject_Instance>>>();
            lecDictionary = new Dictionary<short, Dictionary<int, Dictionary<byte, InstanceCounter>>>();
            Sort(); //Sorts instances from sInstanceStore into their respective 3 dimentional dictionaries
        }


        private void Sort()
        {
            foreach (Subject_Instance sI in sInstanceStore)//loop through all instances
            {
                int mode = sI.mode ? 0 : 1; //dev or teach?
                byte month = sI.Month;
                short year = sI.Year;
                int subjectID = sI.SubjectID;

                if (!getDictionary(mode).ContainsKey(year)) //check if year is null
                {
                    getDictionary(mode).Add(year, new Dictionary<int, Dictionary<byte, Subject_Instance>>()); //add year
                }
                if (!getDictionary(mode)[year].ContainsKey(subjectID)) //is subject ID within year null?
                {
                    getDictionary(mode)[year].Add(subjectID, new Dictionary<byte, Subject_Instance>()); //Add subject ID and month
                }
                getDictionary(mode)[year][subjectID].Add(month, sI); //Add subject instance to month

                if (mode == 0) //teaching - determine low and high month
                {
                    if (teach_lYear == 0 || year < teach_lYear)
                    {
                        teach_lYear = year;
                    }
                    if (teach_hYear == 0  || year > teach_hYear  )
                    {
                        teach_hYear = year;
                    }
                }
                else//developing - determine low and high month
                {
                    if (dev_lYear == 0 || year < dev_lYear)
                    {
                        dev_lYear = year;
                    }
                    if (dev_hYear == 0 || year > dev_hYear)
                    {
                        dev_hYear = year;
                    }
                }



                foreach (Lecturer l in sI.lecturers) //loop through all lecturers in order to count their loads
                {
                    int subjectType = l.subjectTypeID;
                    int userID = l.userID;
                    string StartDate = month.ToString("00") + "/" + year.ToString();
                    bool isPrimary = l.subjectTypeID == 0;
                    byte supportAmount = isPrimary ? Convert.ToByte(sI.lecturers.Count - 1) : Convert.ToByte(0);
                    Instance tempInstance = new Instance(sI.SubjectInstanceID, subjectType, sI.Students, StartDate,isPrimary,supportAmount);
                    short tempYear = year;
                    byte tempMonth = month;
                    for (int i = 0; i < 3; i++)
                    {
                        if (tempMonth > 12)
                        {
                            tempMonth = 1;
                            tempYear++;
                        }
                        if (!lecDictionary.ContainsKey(tempYear))//Same process as listed above. Check for nulls and create needed sub dictionaries
                        {
                            lecDictionary.Add(tempYear, new Dictionary<int, Dictionary<byte, InstanceCounter>>());
                        }
                        if (!lecDictionary[tempYear].ContainsKey(userID))
                        {
                            lecDictionary[tempYear].Add(userID, new Dictionary<byte, InstanceCounter>());
                        }
                        if (!lecDictionary[tempYear][userID].ContainsKey(tempMonth))
                        {
                            lecDictionary[tempYear][userID].Add(tempMonth, new InstanceCounter());
                        }
                        ((InstanceCounter)lecDictionary[tempYear][userID][tempMonth]).instances.Add(tempInstance); // add instance to instance counter for year/userid and month
                        tempMonth++;
                    }
                }
            }
        }

        public Subject_Instance getInstance(int mode, short year, int subjectID, byte month)  //null check and return associated instance at mode/year/subjectID/month
        {
           if (getDictionary(mode).ContainsKey(year) && getDictionary(mode)[year].ContainsKey(subjectID) && getDictionary(mode)[year][subjectID].ContainsKey(month))
            {
                return (Subject_Instance)getDictionary(mode)[year][subjectID][month];
            }
            return null;
        }

        public InstanceCounter getLecInstance(short year, int userID, byte month) // null check and return instance counter at year/userid/month
        {
            if (lecDictionary.ContainsKey(year) && lecDictionary[year].ContainsKey(userID) && lecDictionary[year][userID].ContainsKey(month))
            {
                return (InstanceCounter)lecDictionary[year][userID][month];
            }
            return null;
        }
        

        public Dictionary<short, Dictionary<int, Dictionary<byte, Subject_Instance>>> getDictionary(int mode) //dictionary finder returns mode
        {
            return mode == 0 ? teachDictionary : devDictionary;
        }

        public short getLowYear(bool mode) //gets low year for associated mode
        {
            return mode ? teach_lYear : dev_lYear;
        }

        public short getHighYear(bool mode) //gets high year for associated mode
        {
            return mode ? teach_hYear : dev_hYear;
        }


    }
}