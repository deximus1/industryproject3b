﻿using IndustryProject3A.Classes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

// @Author Luke Dunning

namespace IndustryProject3A
{
    public partial class Users : System.Web.UI.Page
    {
        List<User> userStore = new List<User>();
        List<Subject> subjectStore = new List<Subject>();
        State stateMachine;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadUsers();
                Lists.loadListBox(userStore, userList);
                loadSubjects();
                Lists.loadCheckBoxList(subjectStore, newUser_CheckBoxList_Subjects);
                Lists.loadCheckBoxList(subjectStore, editUser_CheckBoxList_Subjects);
                Session["State"] = stateMachine = new State();
            }
            else
            {
                userStore = (List<User>)Session["USERS_users"];
                subjectStore = (List<Subject>)Session["USERS_subjects"];
                stateMachine = (State)Session["State"];
            }
        }


        private void loadUsers()
        {
            SQLHandler sql = new SQLHandler();
            if (userStore != null && userStore.Count > 0)
            {
                userStore.Clear();
            }
            userStore = sql.getUsers();
            Session["USERS_users"] = userStore;
        }

        private void loadSubjects()
        {
            SQLHandler sql = new SQLHandler();
            if (subjectStore != null && subjectStore.Count > 0)
            {
                subjectStore.Clear();
            }
            subjectStore = sql.getSubjects();
            Session["USERS_subjectCount"] = subjectStore.Count.ToString();
            Session["USERS_subjects"] = subjectStore;
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            if (!SearchBox.Text.Trim().Equals(""))
            {
                Lists.loadListBox(Lists.getSearchList(userStore, SearchBox.Text.Trim()), userList);
            }
            else
            {
                Lists.loadListBox(userStore, userList);
            }
        }


        protected void newUser_Button_Cancel_Click(object sender, EventArgs e)
        {
            newUser_Label_Error.Visible = false;
            emptyAddUserConntrols();
            ModalPopupExtender2.Hide();
        }

        protected void newUser_Button_Submit_Click(object sender, EventArgs e)
        {
            if (stateMachine.STATE != States.ADDING)
            {
                return;
            }
            SQLHandler sql = new SQLHandler();
            string err = "";
            string userName = newUser_TextBox_Username.Text.Trim();
            string passWord = newUser_TextBox_Password.Text.Trim();
            string load = newUser_Textbox_Load.Text.Trim();
            float convLoad = 1.0f;
            bool validConvert = false;

            if (userName.Equals(""))
            {
                err = Lists.addErrorText(err, "Username must not be empty!");
            }
            else if (userName.Count(x => x == '.') != 1)
            {
                err = Lists.addErrorText(err, "Username format must be 'Firsname.Lastname'!");
            }
            else if (sql.isValueTaken("users", "Username", userName))
            {
                err = Lists.addErrorText(err, "Username is allready use!");
            }
            if (passWord.Equals(""))
            {
                err = Lists.addErrorText(err, "Password must not be empty!");
            }
            else if (passWord.Length < 8)
            {
                err = Lists.addErrorText(err, "Password must be atleast 8 characters long!");
            }
            if (load.Equals(""))
            {
                err = Lists.addErrorText(err, "Load must not be empty!");
            }
            else
            {
                try
                {
                    convLoad = float.Parse(load, CultureInfo.InvariantCulture.NumberFormat);
                    validConvert = true;
                }
                catch (Exception ex)
                {
                    err = Lists.addErrorText(err, "Load must be a number between 0.1 and 1.0");
                }
                if ((validConvert) && !(convLoad >= 0.00f && convLoad <= 1.00))
                {
                    err = Lists.addErrorText(err, "Load must be a value between 0.1 and 1.0");
                }
            }

            if (err.Length > 0)
            {
                newUser_Label_Error.Text = err;
                newUser_Label_Error.Visible = true;
                ModalPopupExtender2.Show();
                return;
            }
            int maxid = sql.getMaxID("users");
            sql.addUser(maxid, userName, passWord, newUser_DropDown_Privillege.SelectedIndex, makeSubjectCSV(newUser_CheckBoxList_Subjects), convLoad);
            newUser_Label_Error.Visible = false;
            loadUsers();
            Lists.loadListBox(userStore, userList);
            emptyAddUserConntrols();
            showMSG("New User", "ID: '" + (maxid + 1) + "' Username: '" + userName + "' has been added sucessfully!");
            UpdatePanel.Update();
            stateMachine.Set(States.STANDBY);
        }

        private string makeSubjectCSV(CheckBoxList control)
        {
            string temp = "";
            for (int i = 0; i < control.Items.Count; i++)
            {
                if (control.Items[i].Selected)
                {
                    if (temp.Length > 0)
                    {
                        temp += ",";
                    }
                    temp += (subjectStore[i].SubjectID);
                }
            }
            return temp;
        }

        protected void newUser_DropDown_Privillege_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeAddPopupVisibility(newUser_DropDown_Privillege.SelectedIndex);
            ModalPopupExtender2.Show();
        }

        private void changeAddPopupVisibility(int priv)
        {
            string subjectCount = (string)Session["USERS_subjectCount"];
            if (priv == 0)
            {
                newUser_Textbox_Load.Text = "1.0";
                newUser_CheckBoxList_Subjects.Visible = true;
                newUser_Textbox_Load.Visible = true;
                newUser_Label_Load.Visible = true;
                newUser_Label_Subjects.Visible = true;
                if (!subjectCount.Equals("0"))
                {
                    newUser_Label_Subjects.Text = "Subjects:";
                    newUser_Div1.Visible = true;
                }
                else
                {
                    newUser_Label_Subjects.Text = "No Subjects in DB!";
                    newUser_Div1.Visible = false;
                }
            }
            else
            {
                newUser_CheckBoxList_Subjects.Visible = false;
                newUser_Label_Subjects.Visible = false;
                newUser_Textbox_Load.Visible = false;
                newUser_Label_Load.Visible = false;
                newUser_Div1.Visible = false;
            }
        }

        private void changeEditPopupVisibility(int priv)
        {
            string subjectCount = (string)Session["USERS_subjectCount"];
            if (priv > 0)
            {
                editUser_CheckBoxList_Subjects.Visible = false;
                editUser_Label_Subjects.Visible = false;
                editUser_Textbox_Load.Visible = false;
                editUser_Label_Load.Visible = false;
                editUser_Label_anualend.Visible = false;
                editUser_Label_anualstart.Visible = false;
                editUser_Textbox_anualend.Visible = false;
                editUser_Textbox_anualstart.Visible = false;
                editUser_Textbox_anualend.Text = "";
                editUser_Textbox_anualstart.Text = "";
                editUser_Label_anual.Visible = false;
                editUser_Div.Visible = false;
                editUser_Div2.Visible = false;
            }
            else
            {
                editUser_CheckBoxList_Subjects.Visible = true;
                editUser_Label_Subjects.Visible = true;
                editUser_Textbox_Load.Visible = true;
                editUser_Label_Load.Visible = true;
                editUser_Label_anualend.Visible = true;
                editUser_Label_anualstart.Visible = true;
                editUser_Textbox_anualend.Visible = true;
                editUser_Textbox_anualstart.Visible = true;
                editUser_Label_anual.Visible = true;
                editUser_Div2.Visible = true;
                if (!subjectCount.Equals("0"))
                {
                    editUser_Label_Subjects.Text = "Subjects:";
                    editUser_Div.Visible = true;
                }
                else
                {
                    editUser_Label_Subjects.Text = "No Subjects in DB!:";
                    editUser_Div.Visible = false;
                }
            }
        }

        protected void editUser_DropDown_Privillege_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeEditPopupVisibility(editUser_DropDown_Privillege.SelectedIndex);
            ModalPopupExtender1.Show();
        }

        protected void editUserCancel_Click(object sender, EventArgs e)
        {
            editUser_Label_Error.Visible = false;
            emptyEditUserConntrols();
            ModalPopupExtender1.Hide();
        }

        protected void editUserSubmit_Click(object sender, EventArgs e)
        {
            SQLHandler sql = new SQLHandler();
            string err = "";
            int index = Lists.getObjectIndex(userStore, userList.SelectedValue);
            DateTime AnualStart = new DateTime();
            DateTime AnualEnd = new DateTime();
            string userName = editUser_Textbox_Username.Text.Trim();
            string passWord = editUser_Textbox_Password.Text.Trim();
            string anualStart = editUser_Textbox_anualstart.Text.Trim();
            string anualEnd = editUser_Textbox_anualend.Text.Trim();
            string load = editUser_Textbox_Load.Text.Trim(); ;
            float convLoad = 0.0f;
            bool validConvert = false;
            bool loadChange = (!load.Equals(userStore[index].Load.ToString()));
            int Priv = editUser_DropDown_Privillege.SelectedIndex;
            string subjectCSV = makeSubjectCSV(editUser_CheckBoxList_Subjects);
            bool usernameChange = (!userName.Equals(userStore[index].Username));
            bool passWordChange = (!passWord.Equals(userStore[index].Password));
            bool anualStartChange = (!anualStart.Equals(userStore[index].AnualStart.ToString().Equals("") ? userStore[index].AnualStart.ToString("dd/MM/yyyy") : ""));
            bool anualEndChange = (!anualEnd.Equals(userStore[index].AnualEnd.ToString().Equals("") ? userStore[index].AnualEnd.ToString("dd/MM/yyyy") : ""));
            bool privChange = (Priv != (userStore[index].Privillege));
            bool subjectCSVChange = (!subjectCSV.Equals(userStore[index].AbleSubjects));

            if (usernameChange || passWordChange || anualStartChange || anualEndChange || loadChange || privChange || subjectCSVChange)
            {
                if (stateMachine.STATE != States.EDITING)
                {
                    return;
                }
                if (userName.Equals(""))
                {
                    err = Lists.addErrorText(err, "Username format must be 'Firsname.Lastname'!");
                }
                else if (userName.Count(x => x == '.') != 1)
                {
                    err = Lists.addErrorText(err, "Username should be: Firsname.Lastname!");
                }
                else if (usernameChange && sql.isValueTaken("users", "Username", userName))
                {
                    err = Lists.addErrorText(err, "Username is allready use!");
                }
                if (passWord.Equals(""))
                {
                    err = Lists.addErrorText(err, "Password must not be empty!");
                }
                else if (passWord.Length < 8)
                {
                    err = Lists.addErrorText(err, "Password must be atleast 8 characters long!");
                }
                if (!anualStart.Equals("") && !anualEnd.Equals(""))
                {
                    try
                    {
                        AnualStart = DateTime.ParseExact(anualStart, "dd/MM/yyyy", null);
                        AnualEnd = DateTime.ParseExact(anualEnd, "dd/MM/yyyy", null);
                        if (AnualStart >= AnualEnd)
                        {
                            err = Lists.addErrorText(err, "Anual end date must be after the start date!");
                        }
                    }
                    catch (Exception ex)
                    {
                        err = Lists.addErrorText(err, "Invalid date format! use dd/MM/yyyy");
                    }
                }
                else if (anualStart.Equals("") && !anualEnd.Equals(""))
                {
                    err = Lists.addErrorText(err, "Anual leave must have a start date! ");
                }
                else if (anualEnd.Equals("") && !anualStart.Equals(""))
                {
                    err = Lists.addErrorText(err, "Anual leave must have a end date! ");
                }
                if (load.Equals(""))
                {
                    err = Lists.addErrorText(err, "Load must not be empty!");
                }
                else
                {
                    try
                    {
                        convLoad = float.Parse(load, CultureInfo.InvariantCulture.NumberFormat);
                        validConvert = true;
                    }
                    catch (Exception ex)
                    {
                        err = Lists.addErrorText(err, "Load must be a number between 0.1 and 1.0");
                    }
                    if ((validConvert) && !(convLoad >= 0.00f && convLoad <= 1.00))
                    {
                        err = Lists.addErrorText(err, "Load must be a value between 0.1 and 1.0");
                    }
                }

                if (err.Length > 0)
                {
                    editUser_Label_Error.Text = err;
                    editUser_Label_Error.Visible = true;
                    ModalPopupExtender1.Show();
                    return;
                }
                sql.editUser(userStore[index].UserID, userName, passWord, Priv, subjectCSV, convLoad, ((!anualStart.Equals("") && !anualEnd.Equals("")) ? (AnualStart.Date.ToString("dd/MM/yyyy") + "," + AnualEnd.Date.ToString("dd/MM/yyyy")) : ""));
                loadUsers();
                if (usernameChange)
                {
                    Lists.loadListBox(userStore, userList);
                }
                showMSG("User Edited", "Changes made to ID: '" + userStore[index].UserID + "' Username: '" + userName + "' have been sucessfuly saved!");
            }
            editUser_Label_Error.Visible = false;
            emptyEditUserConntrols();
            stateMachine.Set(States.STANDBY);
            UpdatePanel.Update();
        }

        protected void editUser_Click(object sender, EventArgs e)
        {
            int userIndex = Lists.getObjectIndex(userStore, userList.SelectedValue);
            if (userIndex == -1)
            {
                return;
            }
            stateMachine.Set(States.EDITING);
            ModalPopupExtender1.Show();
            changeEditPopupVisibility(userStore[userIndex].Privillege);
            editUser_Textbox_Username.Text = userStore[userIndex].Username;
            editUser_Textbox_Password.Text = userStore[userIndex].Password;
            editUser_DropDown_Privillege.SelectedIndex = userStore[userIndex].Privillege;
            editUser_Textbox_Load.Text = userStore[userIndex].Load.ToString("0.00");
            if (userStore[userIndex].Privillege == 0)
            {
                if (userStore[userIndex].hasAnualLeave())
                {
                    editUser_Textbox_anualstart.Text = userStore[userIndex].AnualStart.ToString("dd/MM/yyyy");
                    editUser_Textbox_anualend.Text = userStore[userIndex].AnualEnd.ToString("dd/MM/yyyy");
                }
                loadEditCheckBoxes(userIndex);
            }
            else
            {
                editUser_Textbox_anualstart.Text = "";
                editUser_Textbox_anualend.Text = "";
            }
        }


        private void loadEditCheckBoxes(int userIndex)
        {
            editUser_CheckBoxList_Subjects.SelectedIndex = -1;
            int[] onepass = new int[400];
            for (int i = 0; i < userStore[userIndex].AbleSubjectList.Count; i++)
            {
                onepass[userStore[userIndex].AbleSubjectList[i]] = 1;
            }
            for (int i = 0; i < subjectStore.Count; i++)
            {
                if (onepass[subjectStore[i].SubjectID] == 1)
                {
                    editUser_CheckBoxList_Subjects.Items[i].Selected = true;
                }
            }
        }

        protected void addUser_Click(object sender, EventArgs e)
        {
            changeAddPopupVisibility(0);
            ModalPopupExtender2.Show();
            stateMachine.Set(States.ADDING);
        }

        private void emptyAddUserConntrols()
        {
            newUser_TextBox_Username.Text = "";
            newUser_TextBox_Password.Text = "";
            newUser_DropDown_Privillege.SelectedIndex = 0;
            newUser_Textbox_Load.Text = "1.00";
            newUser_CheckBoxList_Subjects.ClearSelection();
        }

        private void emptyEditUserConntrols()
        {
            editUser_Textbox_Username.Text = "";
            editUser_Textbox_Password.Text = "";
            editUser_DropDown_Privillege.SelectedIndex = 0;
            editUser_Textbox_Load.Text = "1.00";
            editUser_Textbox_anualstart.Text = "";
            editUser_Textbox_anualend.Text = "";
            editUser_CheckBoxList_Subjects.ClearSelection();
        }

        protected void deleteUser_Click(object sender, EventArgs e)
        {
            int index = Lists.getObjectIndex(userStore, userList.SelectedValue);
            if (index == -1)
            {
                return;
            }
            stateMachine.Set(States.DELETING);
            int id = userStore[index].UserID;
            if (!isUserLoggedIn(index))
            {
                deleteUser_Label.Text = "Are you sure you want to delete: [" + userStore[index].getPrivillegeName() + "] ID: '" + id + "' Username: '" + userStore[index].Username + "'? Warning this will remove the user from all instances!";
                ModalPopupExtender3.Show();
            }
            else
            {
                showMSG("Error", "You cannot delete Username: '" + userStore[index].Username + "' because you are currently using this account!");
            }
        }

        private bool isUserLoggedIn(int index)
        {
            return Session["Username"].Equals(userStore[index].Username);
        }

        protected void deleteUser_Button_Yes_Click(object sender, EventArgs e)
        {
            if (stateMachine.STATE != States.DELETING)
            {
                return;
            }
            stateMachine.Set(States.STANDBY);
            int index = Lists.getObjectIndex(userStore, userList.SelectedValue);
            int id = userStore[index].UserID;
            string username = userStore[index].Username;
            SQLHandler sql = new SQLHandler();
            sql.deleteRow("users", id);
            loadUsers();
            Lists.loadListBox(userStore, userList);

            List<Subject_Instance> sInstanceStore = sql.getSubjectInstances();
            foreach (Subject_Instance sI in sInstanceStore) // removes all user associations in the instance DB
            {
                for (int i = 0; i < sI.lecturers.Count; i++)
                {
                    if (sI.lecturers[i].userID == id)
                    {
                        sI.lecturers.RemoveAt(i);
                        string newLecturers = "";
                        foreach (Lecturer lec in sI.lecturers)
                        {
                            if (newLecturers.Length > 0)
                            {
                                newLecturers += ",";
                            }
                            newLecturers += lec.userID + "|" + lec.subjectTypeID;
                        }
                        sql.editInstanceLecturers(sI.SubjectInstanceID, newLecturers);
                        break;
                    }
                }
            }
            UpdatePanel.Update();
            showMSG("User Deleted", "User - ID: '" + id + "' Username: '" + username + "' has been deleted sucessfully! All instances with this user have been modified!");
        }

        private void showMSG(string title, string text)
        {
            messageBox_label_title.Text = title;
            messageBox_label_contents.Text = text;
            ModalPopupExtender4.Show();
        }
    }
}