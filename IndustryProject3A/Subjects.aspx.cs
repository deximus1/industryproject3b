﻿using IndustryProject3A.Classes;
using System;
using System.Collections.Generic;

// @Author Luke Dunning

namespace IndustryProject3A
{
    public partial class Subjects : System.Web.UI.Page
    {

        List<Subject> subjectStore = new List<Subject>();
        State stateMachine;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadSubjects();
                Lists.loadListBox(subjectStore, subjectList);
                Session["State"] = stateMachine = new State();
            }
            else
            {
                subjectStore = (List<Subject>)Session["SUBJECTS_subjects"];
                stateMachine = (State)Session["State"];
            }
        }

        private void loadSubjects()
        {
            SQLHandler sql = new SQLHandler();
            if (subjectStore != null && subjectStore.Count > 0)
            {
                subjectStore.Clear();
            }
            subjectStore = sql.getSubjects();
            Session["SUBJECTS_subjects"] = subjectStore;
        }


        protected void addSubject_Click(object sender, EventArgs e)
        {
            ModalPopupExtender2.Show();
            stateMachine.Set(States.ADDING);
        }

        protected void newSubject_Button_Submit_Click(object sender, EventArgs e)
        {
            if (stateMachine.STATE != States.ADDING)
            {
                return;
            }
            SQLHandler sql = new SQLHandler();
            string err = "";
            string name = newSubject_TextBox_Name.Text.Trim();
            string unitID = newSubject_TextBox_UnitID.Text.Trim();

            if (unitID.Equals(""))
            {
                err = Lists.addErrorText(err, "UnitID must not be empty!");
            }
            else if (sql.isValueTaken("subjects", "UnitID", unitID))
            {
                err = Lists.addErrorText(err, "UnitID allready in use!");
            }
            if (name.Equals(""))
            {
                err = Lists.addErrorText(err, "Name must not be empty!");
            }
            else if(sql.isValueTaken("subjects", "Name", name))
            {
                err = Lists.addErrorText(err, "Name allready in use!");
            }           
            if (err.Length > 0)
            {
                newSubject_Label_Error.Text = err;
                newSubject_Label_Error.Visible = true;
                ModalPopupExtender2.Show();
                return;
            }
            stateMachine.Set(States.STANDBY);
            int maxid = sql.getMaxID("subjects");
            sql.addSubject(maxid, name, unitID);
            newSubject_Label_Error.Visible = false;
            loadSubjects();
            Lists.loadListBox(subjectStore, subjectList);
            emptyAddSubjectControls();
            UpdatePanel.Update();
            showMSG("New Subject", "ID: '" + (maxid + 1) + "' Name: '" + name + "' has been added sucessfully!");
        }

        protected void newSubject_Button_Cancel_Click(object sender, EventArgs e)
        {
            emptyAddSubjectControls();
            ModalPopupExtender2.Hide();
        }

        private void emptyAddSubjectControls()
        {
            newSubject_TextBox_Name.Text = "";
            newSubject_TextBox_UnitID.Text = "";
            newSubject_Label_Error.Text = "";
            newSubject_Label_Error.Visible = false;
        }

        private void emptyEditSubjectControls()
        {
            editSubject_Textbox_Name.Text = "";
            editSubject_Textbox_UnitID.Text = "";
            editSubject_Label_Error.Text = "";
            editSubject_Label_Error.Visible = false;
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            if (!SearchBox.Text.Trim().Equals(""))
            {
                Lists.loadListBox(Lists.getSearchList(subjectStore,SearchBox.Text), subjectList);
            }
            else
            {
                Lists.loadListBox(subjectStore, subjectList);
            }
        }

        protected void editSubject_Click(object sender, EventArgs e)
        {
            int index = Lists.getObjectIndex(subjectStore,subjectList.SelectedValue);
            if (index == -1)
            {
                return;
            }
            stateMachine.Set(States.EDITING);
            ModalPopupExtender1.Show();
            editSubject_Textbox_Name.Text = subjectStore[index].Name;
            editSubject_Textbox_UnitID.Text = subjectStore[index].UnitID;
        }

        protected void editSubject_Button_Cancel_Click(object sender, EventArgs e)
        {
            emptyEditSubjectControls();
            ModalPopupExtender1.Hide();
        }

        protected void editSubject_Button_Submit_Click(object sender, EventArgs e)
        {
            if (stateMachine.STATE != States.EDITING)
            {
                return;
            }
            SQLHandler sql = new SQLHandler();
            string err = "";
            int index = Lists.getObjectIndex(subjectStore, subjectList.SelectedValue);
            string name = editSubject_Textbox_Name.Text.Trim();
            string unitID = editSubject_Textbox_UnitID.Text.Trim();
            bool nameChange = (!name.Equals(subjectStore[index].Name));
            bool unitIDChange = (!unitID.Equals(subjectStore[index].UnitID));

            if (nameChange || unitIDChange)
            {
                if (name.Equals(""))
                {
                    err = Lists.addErrorText(err, "Name must not be empty!");
                }
                else if (nameChange && sql.isValueTaken("subjects", "Name", name))
                {
                    err = Lists.addErrorText(err, "Name allready in use!");
                }
                if (unitID.Trim().Equals(""))
                {
                    err = Lists.addErrorText(err, "UnitID must not be empty!");
                }
                else if (unitIDChange && sql.isValueTaken("subjects", "UnitID", unitID))
                {
                    err = Lists.addErrorText(err, "UnitID allready in use!");
                }

                if (err.Length > 0)
                {
                    editSubject_Label_Error.Text = err;
                    editSubject_Label_Error.Visible = true;
                    ModalPopupExtender1.Show();
                    return;
                }
                stateMachine.Set(States.STANDBY);

                sql.editSubject(subjectStore[index].SubjectID, name, unitID);
                loadSubjects();
                if (nameChange)
                {
                    Lists.loadListBox(subjectStore, subjectList);
                }
                showMSG("Edit Subject", "Changes made to ID: '" + subjectStore[index].SubjectID + "' Name: '" + name + "' have been sucessfully saved!");
            }
            if (stateMachine.STATE == States.EDITING)
            {
                stateMachine.Set(States.STANDBY);
            }
            editSubject_Label_Error.Visible = false;
            emptyEditSubjectControls();
            UpdatePanel.Update();
        }

        protected void deleteSubject_Click(object sender, EventArgs e)
        {
            int index = Lists.getObjectIndex(subjectStore, subjectList.SelectedValue);
            if (index == -1)
            {
                return;
            }
            stateMachine.Set(States.DELETING);
            ModalPopupExtender3.Show();
            deleteSubject_Label.Text = "Are you sure you want to delete: " + subjectStore[index].Name + "? !WARNING! This will delete all instances associated to this Subject!";
        }

        protected void deleteSubject_Button_Yes_Click(object sender, EventArgs e)
        {
            if (stateMachine.STATE != States.DELETING)
            {
                return;
            }
            stateMachine.Set(States.STANDBY);
            SQLHandler sql = new SQLHandler();
            int index = Lists.getObjectIndex(subjectStore, subjectList.SelectedValue);
            int id = subjectStore[index].SubjectID;
            string name = subjectStore[index].Name;
            sql.deleteRow("subjects", id);
            loadSubjects();
            Lists.loadListBox(subjectStore, subjectList);
            sql.deleteAllRow("subjectinstances", "SubjectID", id);

            List<User> userStore = sql.getUsers();

            foreach (User u in userStore)
            {
               if (u.AbleSubjectList.Contains(id))
                {
                    u.AbleSubjectList.Remove(id);
                    u.convertConvertSubjectString();
                    sql.editUserAbleSubjects(u.UserID, u.AbleSubjects);
                }
            }
            UpdatePanel.Update();
            showMSG("Delete Subject", " Subject - ID: '" + id + "' Name: '"+name+ "' and associated instances have been deleted sucessfully");
        }
        private void showMSG(string title, string text)
        {
            messageBox_label_title.Text = title;
            messageBox_label_contents.Text = text;
            ModalPopupExtender4.Show();
        }
    }
}