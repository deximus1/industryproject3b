﻿<%@ Page Title="Instance Management" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="CreateInstance.aspx.cs" Inherits="IndustryProject3A.WebForm2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%-- Inline Style --%>
    <style>
        .modalBackground {
            background-color: black;
            filter: alpha(opacity=0.9) !important;
            opacity: 0.6 !important;
            z-index: 20;
        }

        .modalPopup {
            padding: 20px 10px 24px 10px;
            position: relative;
            width: auto;
            height: auto;
            background-color: #004A80;
            border: 1px solid black;
            top: 0px;
            left: 1px;
        }

        .modalBackdrop {
            z-index: -1;
        }

        .errorText {
            padding: 2px 2px 2px 2px;
            position: relative;
            background-color: lightcoral;
            border: 1px solid red;
        }
    </style>

    <%-- Title Image --%>
    <div class="Jumbotron">
        <br />
        <img src="Images/Banner.png" />
    </div>

    <%-- Page Title --%>
    <div class="col-md-11">
        <center><h2>Instance Management</h2></center>

        <asp:Panel ID="Panel_Instance" runat="server">
          
            <center>
                <h2><b><asp:Label id="Label_error" runat="server" text=""></asp:Label></b></h2>
                <div id="SearchTable" runat="server">
                    <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%;">
                        <tr>
                            <asp:Label ID="search_Label" runat="server" Text="Label">Subject Name: </asp:Label>
                            <asp:TextBox ID="search_Textbox" runat="server"></asp:TextBox><asp:Button ID="search_Button" runat="server" Text ="Search" OnClick="search_Button_Click" />
                            <caption>
                                &nbsp;
                            </caption>
                        </tr>   
                    </table> 
                    </div>
                </center>
        </asp:Panel>
                 <center>
            <asp:Panel ID="Instance_Legend" runat="server">
                <asp:LinkButton ID="LinkButton_Legend" runat="server" Text="Legend" OnClick="LinkButton_Legend_Click"></asp:LinkButton></strong>
                <asp:GridView ID="GridView_Legend" AutoGenerateColumns="true" ShowHeaderWhenEmpty="true" runat="server" OnDataBound="GridView_Legend_DataBound" ShowHeader="False" HorizontalAlign="Center"></asp:GridView>
            </asp:Panel>
        </center>
        <br />

        <%-- Page Content --%>
        <center>
            <asp:Button ID="Button_Teaching" runat="server" Text="Teaching View" CssClass="viewButton" OnClick="Button_Teaching_Click" />&nbsp;<asp:Button ID="Button_Developing" runat="server" Text="Developing View" OnClick="Button_Developing_Click" />&nbsp;<asp:Button ID="Button_Lecturer" runat="server" Text="Lecturer View" OnClick="Button_Lecturer_Click" />
        </center>
        <br />
            <div class="col-lg-12">
                <center>
                <asp:UpdatePanel ID="UpdatePanel" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server">
                    <ContentTemplate>
                    <center><asp:Button ID="Button_Year1_Down" runat="server" Text="←" CssClass="viewButton" OnClick="Button_Year1_Down_Click" /><asp:Button ID="Button_Year1_Up" runat="server" Text="→" CssClass="viewButton" OnClick="Button_Year1_Up_Click" />&nbsp;<asp:Label ID="Label_Year" runat="server"></asp:Label> &nbsp;<asp:Button ID="Button_Year2_Down" runat="server" Text="←" OnClick="Button_Year2_Down_Click" /><asp:Button ID="Button_Year2_Up" runat="server" Text="→" OnClick="Button_Year2_Up_Click" /></center>
                        <br />
                        <div id="scrollDiv" style="OVERFLOW-Y:auto; WIDTH:100%; HEIGHT:80%">
                            <asp:HiddenField ID="SelectedGridCellIndex" runat="server" Value="-1" />
                            <asp:GridView ID="GridView_Instances" AutoGenerateColumns="true" ShowHeaderWhenEmpty="true" runat="server" EmptyDataText="No Records found!" OnDataBound="GridView_Instances_DataBound" OnRowCreated="GridView_Instances_RowCreated" OnSelectedIndexChanged="GridView_Instances_SelectedIndexChanged" OnRowDataBound="GridView_Instances_RowDataBound" OnRowUpdating="GridView_Instances_RowUpdating" RowStyle-BackColor="white" RowStyle-Wrap="False">
                                <RowStyle BackColor="White" Wrap="false"/>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </center>
            </div>
        <br />
        <br />
        <br />
    </div>

    <asp:UpdatePanel ID="Popup_Instance" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server" CssClass="modalPopup">
        <ContentTemplate>
            <asp:Panel ID="Popup_Instance_Main" CssClass="modalPopup" runat="server">
                <center><b><h2 style="color:white"><asp:Label ID="Popup_Instance_Label_Title" runat="server" Text="SubjectID and Date"></asp:Label></h2></b></center>
                <center>
                <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: auto; height: auto;">
                    <tr>
                        <td>&nbsp;&nbsp; &nbsp;
                        </td>

                        <td>
                            <asp:Button ID="Popup_Instance_Button_Add" runat="server" Text="Add" OnClick="Popup_Instance_Button_Add_Click" />
                            <br />
                            <asp:Button ID="Popup_Instance_Button_Edit" runat="server" Text="Edit" Enabled="False" OnClick="Popup_Instance_Button_Edit_Click" />
                            <br />
                            <asp:Button ID="Popup_Instance_Button_Remove" runat="server" Text="Remove" Enabled="False" OnClick="Popup_Instance_Button_Remove_Click" />
                            <br />
                            <br />
                            <br />
                            <asp:Button ID="Popup_Instance_Button_DeleteInstance" runat="server" Text="Delete" Enabled="false" OnClick="Popup_Instance_Button_DeleteInstance_Click" />
                        </td>
                        <td>&nbsp;&nbsp; &nbsp;
                        </td>

                        <td>&nbsp;
                            <br />
                            Added Users:
                            <br />
                            <asp:ListBox ID="Popup_Instance_Listbox_AddedUsers" AutoPostBack="true" runat="server" style=" height: 250px; width: 800px;"></asp:ListBox>
                            <br />
                            <div id="Popup_Instance_Div" runat="server">
                            <asp:Label ID="Popup_Instance_Label_Students" runat="server">Number of Students:</asp:Label>
                            <br />
                            <asp:TextBox ID="Popup_Instance_Textbox_Students" runat="server" MaxLength="5"></asp:TextBox>
                            </div>
                            <br />
                            <br />
                            <asp:Label ID="Popup_Instance_Error" runat="server" CssClass="errorText" Visible="false"></asp:Label>
                        </td>

                        <td>&nbsp;&nbsp; &nbsp;
                        </td>
                    </tr>
                </table>
                    </center>
                <br />
                <center>
                    <asp:Button ID="Popup_Instance_Button_Ok" runat="server" Text="Ok" OnClick="Popup_Instance_Button_Ok_Click"/>&nbsp;&nbsp;<asp:Button ID="Popup_Instance_Button_Cancel" runat="server" Text="Cancel" OnClick="Popup_Instance_Button_Cancel_Click"/>
                </center>
            </asp:Panel>

            <%-- Popups --%>
            <%-- Instance Popup --%>
            <asp:Panel ID="Popup_Instance_User" CssClass="modalPopup" runat="server" Visible="false">
                <center><b><h2 style="color:white"><asp:Label ID="Popup_Instance_User_Label_Title" runat="server" Text="SubjectID and Date"></asp:Label></h2></b></center>
                <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%;">
                    <tr>
                        <td>
                            <center>
                            <asp:TextBox ID="Popup_Instance_User_TextBox_SearchUser" runat="server"></asp:TextBox>
                            <asp:Button ID="Popup_Instance_User_Button_SearchUser" runat="server" Text="Search" OnClick="Popup_Instance_User_Button_SearchUser_Click" />
                            <br />
                                <br />
                            User Role:
                            <br />
                            <asp:DropDownList ID="Popup_Instance_User_Type" runat="server"></asp:DropDownList>
                                <br />
                                <br />
                                <asp:Label ID="Popup_Instance_User_Error" runat="server" CssClass="errorText" Visible="false"></asp:Label>
                                </center>
                        </td>
                        <td>
                            <br />
                            Available Lecturers:
                            <br />
                            <asp:ListBox ID="Popup_Instance_User_Listbox_SearchUsers" AutoPostBack="true" runat="server" Height="250px" Width="800px"></asp:ListBox>
                            <br />
                            <br />
                        </td>
                        <td>&nbsp;&nbsp; &nbsp;</td>
                    </tr>
                </table>
                <center>
                    <asp:Button ID="Popup_Instance_User_Button_Ok" runat="server" Text="Ok" OnClick="Popup_Instance_User_Button_Ok_Click"/>&nbsp;&nbsp;<asp:Button ID="Popup_Instance_User_Button_Cancel" runat="server" Text="Cancel" OnClick="Popup_Instance_User_Button_Cancel_Click"/>
                       
                </center>
            </asp:Panel>

            <%-- Confirm Delete Panel --%>
            <asp:Panel ID="Popup_Instance_Delete" runat="server" CssClass="modalPopup" Visible="false">
                <center><b><h2 style="color:white">Confirm Delete</h2></b></center>
                <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
                    <tr>
                        <td width="100%">
                            <center><asp:Label ID="Popup_Instance_Delete_Label" runat="server"></asp:Label></center>
                            <center><asp:Button ID="Popup_Instance_Delete_Button_Yes" runat="server" Text="Yes" OnClick="Popup_Instance_Delete_Button_Yes_Click"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="Popup_Instance_Delete_Button_No" runat="server" Text="No" OnClick="Popup_Instance_Delete_Button_No_Click"/></center>
                        </td>
                    </tr>
                </table>
            </asp:Panel>


            <%-- Message Box Panel --%>
            <asp:Panel ID="Popup_Instance_messageBox" runat="server" CssClass="modalPopup" Visible="false">
                <center><b><h2 style="color:white"><asp:Label ID="Popup_Instance_messageBox_label_title" runat="server" Text="Title"></asp:Label></h2></b></center>
                <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%; height: auto">
                    <tr>
                        <td width="auto">
                            <asp:Label ID="Popup_Instance_messageBox_label_contents" runat="server" Text="subject_Messagebox"></asp:Label>
                            <center><asp:Button ID="Popup_Instance_messageBox_Button_Ok" runat="server" Text="Ok" OnClick="Popup_Instance_messageBox_Button_Ok_Click"/></center>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Popup_Instance" TargetControlID="btnMockInstance" CancelControlID="Popup_Instance_Button_Cancel" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>


        <asp:UpdatePanel ID="Popup_Lecturer" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server" CssClass="modalPopup">
            <ContentTemplate>
                <asp:Panel ID="Panel_Lecturers" CssClass="modalPopup" runat="server">
                <center><b><h2 style="color:white"><asp:Label ID="Popup_Lecturer_Label_Title" runat="server" Text="Baku"></asp:Label></h2></b></center>
                <center>
                    <table border="0" cellspacing="0" cellpadding="0" style="background-color: white; width: 98%;">
                        <tr>
                        <td>&nbsp;&nbsp; &nbsp;
                    <td width="auto">
                        <br />
                    <asp:GridView ID="Popup_Lecturer_GridView" AutoGenerateColumns="true" ShowHeaderWhenEmpty="true" runat="server" EmptyDataText="No Records found!" RowStyle-BackColor="white" RowStyle-Wrap="False" OnDataBound="Popup_Lecturer_GridView_DataBound" OnRowCreated="Popup_Lecturer_GridView_RowCreated">
                    <RowStyle BackColor="White" Wrap="false"/>
                    </asp:GridView>
                        <br />
                    </td>
                        <td>&nbsp;&nbsp; &nbsp;
                    </tr>
                </table>
                   
                    </center>
                <center>
                    <asp:Button ID="Popup_Lecturer_Button_Ok" runat="server" Text="Ok" OnClick="Popup_Lecturer_Button_Ok_Click"/>
                </center>
            </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" PopupControlID="Popup_Lecturer" TargetControlID="btnMockLecturer" CancelControlID="btnMockLecturerOk" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>

    <asp:Button ID="btnMockInstance" TabIndex="-1" runat="server" Text="" Style="visibility: hidden; display: none;" />
    <asp:Button ID="btnMockMSG" TabIndex="-1" runat="server" Text="" Style="visibility: hidden; display: none;" />
    <asp:Button ID="btnMockMSGOK" TabIndex="-1" runat="server" Text="" Style="visibility: hidden; display: none;" />
    <asp:Button ID="btnMockDelete" TabIndex="-1" runat="server" Text="" Style="visibility: hidden; display: none;" />
    <asp:Button ID="btnMockLecturer" TabIndex="-1" runat="server" Text="" Style="visibility: hidden; display: none;" />
    <asp:Button ID="btnMockLecturerOk" TabIndex="-1" runat="server" Text="" Style="visibility: hidden; display: none;" />

    <script type="text/javascript"> 
        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            xPos = $get('scrollDiv').scrollLeft;
            yPos = $get('scrollDiv').scrollTop;
        }
        function EndRequestHandler(sender, args) {
            $get('scrollDiv').scrollLeft = xPos;
            $get('scrollDiv').scrollTop = yPos;
        }
    </script>
</asp:Content>
