﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using IndustryProject3A.Classes;

// @Author Luke Dunning

namespace IndustryProject3A
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        InstanceSorter instanceSorter;
        List<User> userStore = new List<User>();
        List<User> lecUserStore = new List<User>();
        List<Subject> subjectStore = new List<Subject>();
        List<Subject_Type> sTypeStore = new List<Subject_Type>();

        List<User> selectedUsers = new List<User>();
        List<Subject> selectedSubjects = new List<Subject>();

        State stateMachine;
        Settings settings;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SQLHandler sql = new SQLHandler();
                userStore = selectedUsers = sql.getUsers();
                subjectStore = selectedSubjects = sql.getSubjects();
                sTypeStore = sql.getSubjectTypes();
                lecUserStore = getLecturerUsers();
                int uIndex = Lists.getObjectIndex(userStore, (string)Session["Username"]);
                instanceSorter = new InstanceSorter(sql.getSubjectInstances());
                settings = sql.getSettings();
                Session["INSTANCEMANAGER_instanceSorter"] = instanceSorter;
                Session["INSTANCEMANAGER_lecUserStore"] = lecUserStore;
                Session["INSTANCEMANAGER_userStore"] = userStore;
                Session["INSTANCEMANAGER_subjectStore"] = subjectStore;
                Session["INSTANCEMANAGER_sTypeStore"] = sTypeStore;
                Session["INSTANCEMANAGER_selectedUsers"] = selectedUsers;
                Session["INSTANCEMANAGER_selectedSubjects"] = selectedSubjects;
                Session["INSTANCEMANAGER_settings"] = settings;
                ViewState["Sort"] = userStore[uIndex].Sort;
                ViewState["yearSelect_l"] = userStore[uIndex].lYear == 0 ? instanceSorter.teach_lYear : userStore[uIndex].lYear;
                ViewState["yearSelect_h"] = userStore[uIndex].hYear == 0 ? instanceSorter.teach_hYear : userStore[uIndex].hYear;
                Session["INSTANCEMANAGER_mode"] = userStore[uIndex].mode;
                Session["State"] = stateMachine = new State();

                if (checkErrors())
                {
                    Button_Developing.Visible = false;
                    Button_Teaching.Visible = false;
                    Button_Lecturer.Visible = false;
                    UpdatePanel.Visible = false;
                    SearchTable.Visible = false;
                    return;
                }


                updateYearRangeButtons();
                buildGridView(true);
                updateViewButtons();
            }
            else
            {
                lecUserStore = (List<User>)Session["INSTANCEMANAGER_lecUserStore"];
                instanceSorter = (InstanceSorter)Session["INSTANCEMANAGER_instanceSorter"];
                userStore = (List<User>)Session["INSTANCEMANAGER_userStore"];
                subjectStore = (List<Subject>)Session["INSTANCEMANAGER_subjectStore"];
                sTypeStore = (List<Subject_Type>)Session["INSTANCEMANAGER_sTypeStore"];
                selectedUsers = (List<User>)Session["INSTANCEMANAGER_selectedUsers"];
                selectedSubjects = (List<Subject>)Session["INSTANCEMANAGER_selectedSubjects"];
                stateMachine = (State)Session["State"];
                settings = (Settings)Session["INSTANCEMANAGER_settings"];
            }
        }

        private bool checkErrors() //Bootup error checking
        {
            if (subjectStore.Count == 0)
            {
                Label_error.Text = "There are no records in the subject table!";
                return true;
            }
            foreach (User u in userStore)
            {
                if (u.Privillege == 0)
                {
                    return false;
                }
            }
            Label_error.Text = "There are no lecturers in the user table!";
            return true;
        }

        private void updateViewButtons()
        {
            int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"].ToString());
            if (mode == 0)
            {
                Button_Teaching.Enabled = false;
                Button_Developing.Enabled = true;
                Button_Lecturer.Enabled = true;
            }else if (mode == 1)
            {
                Button_Teaching.Enabled = true;
                Button_Developing.Enabled = false;
                Button_Lecturer.Enabled = true;
            }
            else
            {
                Button_Teaching.Enabled = true;
                Button_Developing.Enabled = true;
                Button_Lecturer.Enabled = false;
            }
        }

        private void updateYearRangeButtons()
        {
            int lYear = Convert.ToInt32(ViewState["yearSelect_l"]);
            int hYear = Convert.ToInt32(ViewState["yearSelect_h"]);

            if (lYear == hYear)
            {
                Button_Year2_Down.Visible = false;
                Button_Year2_Down.Enabled = false;

                Button_Year1_Up.Visible = false;
                Button_Year1_Up.Enabled = false;
            }
            else
            {
                Button_Year2_Down.Visible = true;
                Button_Year2_Down.Enabled = true;

                Button_Year1_Up.Visible = true;
                Button_Year1_Up.Enabled = true;
            }
        }

        private void setYearRange(int yearIndex, bool up)
        {
            int year = yearIndex == 0 ? Convert.ToInt32(ViewState["yearSelect_l"]) : Convert.ToInt32(ViewState["yearSelect_h"]);
            year = up ? year + 1 : year - 1;
            if (yearIndex == 0)
            {
                ViewState["yearSelect_l"] = year;
            }
            else
            {
                ViewState["yearSelect_h"] = year;
            }
            SQLHandler sql = new SQLHandler();
            sql.editUserYearSelect(Convert.ToInt32(Session["UserID"]), ViewState["yearSelect_l"].ToString() + "," + ViewState["yearSelect_h"].ToString());
            updateYearRangeButtons();
            buildGridView(false);
        }

        private void buildGridView(bool init)
        {
            if (!init)
            {
                SQLHandler sql = new SQLHandler();
                instanceSorter = new InstanceSorter(sql.getSubjectInstances());
                Session["INSTANCEMANAGER_instanceSorter"] = instanceSorter;
            }
            int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]);
            short yearStart = Convert.ToInt16(ViewState["yearSelect_l"]);
            short yearEnd = Convert.ToInt16(ViewState["yearSelect_h"]);
            FillLegend(mode);

            DataTable dt = new DataTable();
            if (mode < 2)
            {
                dt.Columns.AddRange(new DataColumn[2] { new DataColumn("ID"), new DataColumn("Subject Name") });
            }
            else
            {
                dt.Columns.AddRange(new DataColumn[3] { new DataColumn("ID"), new DataColumn("Username"), new DataColumn("Max Load") });
            }
            for (int i = yearStart; i <= yearEnd; i++)
            {
                string yearTag = i.ToString().Substring(2, 2);
                dt.Columns.AddRange(new DataColumn[12] { new DataColumn("Jan/" + yearTag), new DataColumn("Feb/" + yearTag), new DataColumn("Mar/" + yearTag), new DataColumn("Apr/" + yearTag), new DataColumn("May/" + yearTag), new DataColumn("Jun/" + yearTag), new DataColumn("Jul/" + yearTag), new DataColumn("Aug/" + yearTag), new DataColumn("Sep/" + yearTag), new DataColumn("Oct/" + yearTag), new DataColumn("Nov/" + yearTag), new DataColumn("Dec/" + yearTag) });

            }
            int rowindex = 0;
            if (mode < 2)
            {
                foreach (Subject s in selectedSubjects)
                {
                    dt.Rows.Add(s.SubjectID.ToString(), s.UnitID + "-" + s.Name);
                    List<String> instanceInfo = new List<String>();
                    for (short x = yearStart; x <= yearEnd; x++)
                    {
                        for (int i = 1; i <= 12; i++)
                        {
                            byte month = Convert.ToByte(i);
                            string info = "";

                            Subject_Instance tempInstance = (Subject_Instance)instanceSorter.getInstance(mode, x, s.SubjectID, month);
                            if (tempInstance != null)
                            {
                                info = "~";
                            }
                            instanceInfo.Add(info);
                        }
                    }
                    for (int k = 0; k < instanceInfo.Count; k++)
                    {
                        dt.Rows[rowindex][k + 2] = instanceInfo[k];
                    }
                    rowindex++;
                }
            }
            else
            {
                foreach (User u in selectedUsers)
                {
                    if (u.Privillege == 0)
                    {
                        dt.Rows.Add(u.UserID.ToString(), u.Username, ((double)u.Load*settings.loadMax).ToString("0.00"));
                        List<String> instanceInfo = new List<String>();
                        for (short x = yearStart; x <= yearEnd; x++)
                        {
                            for (int i = 1; i <= 12; i++)
                            {
                                byte month = Convert.ToByte(i);
                                string info = "";

                                InstanceCounter tempCounter = (InstanceCounter)instanceSorter.getLecInstance(x, u.UserID, month);
                                if (tempCounter != null)
                                {
                                    info = tempCounter.getTotal(sTypeStore).ToString("0.00");
                                }
                                instanceInfo.Add(info);
                            }
                        }
                        for (int k = 0; k < instanceInfo.Count; k++)
                        {
                            dt.Rows[rowindex][k + 3] = instanceInfo[k];
                        }
                        rowindex++;
                    }
                }
            }
            Session["INSTANCEMANAGER_startYear"] = yearStart;
            Session["INSTANCEMANAGER_dt"] = dt;
            GridView_Instances.DataSource = dt;
            GridView_Instances.DataBind();
            sortGridView(true);
            Label_Year.Text = (yearStart == yearEnd) ? yearStart.ToString() : yearStart.ToString() + " - " + yearEnd.ToString();
            UpdatePanel.Update();
        }

        protected void GridView_Instances_DataBound(object sender, EventArgs e)
        {
            GridView_Instances.HeaderRow.Cells[0].Visible = false; //COMMENT THIS TO SHOW ID

            foreach (GridViewRow gVR in GridView_Instances.Rows)
            {
                    for (int i = 2; i < gVR.Cells.Count; i++)
                    {
                        if (!gVR.Cells[i].Text.Equals("&nbsp;") && !gVR.Cells[i].Text.Equals(""))
                        {
                            updateCell(gVR, i);
                        }
                    }
            }
        }

        private void updateCell(GridViewRow row, int column)
        {
            int startYear = Convert.ToInt32(Session["INSTANCEMANAGER_startYear"]);
            int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]);
            short year = Convert.ToInt16(startYear + ((column - 2) / 12));
            byte month = Convert.ToByte((column - 1) - ((year - startYear) * 12));
            string text = row.Cells[column].Text;

            if (mode < 2 && text.Equals("~"))
            {
                int subjectID = Convert.ToInt32(row.Cells[0].Text);
                Subject_Instance tempInstance = instanceSorter.getInstance(mode, year, subjectID, month);

                bool annualConflict = false;
                foreach(Lecturer l in tempInstance.lecturers)
                {
                    User tempUser = userStore[Lists.getObjectIndex(userStore, l.userID)];
                    if (!tempUser.isAvail(new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1)))
                    {
                        annualConflict = true;
                    }
                }

                if (tempInstance.lecturers.Count == 0)
                {
                    row.Cells[column].BackColor = settings.c_NoLecturer;
                }else if (annualConflict)
                {
                    row.Cells[column].BackColor = settings.c_hasAnnual;
                }
                else if (mode == 0 && !tempInstance.hasPrimary())
                {
                    row.Cells[column].BackColor = settings.c_NoPrimary;
                }
                else
                {
                    row.Cells[column].BackColor = settings.c_Ok;
                }
                row.Cells[column].Text = "";
            }
            else if (mode == 2 && column != 2)
            {
                bool annualConflict = false;
                User tempUser = userStore[Lists.getObjectIndex(userStore, Convert.ToInt32(row.Cells[0].Text))];
                if (!tempUser.isAvail(new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1)))
                {
                    annualConflict = true;
                }

                float val = float.Parse(text, CultureInfo.InvariantCulture.NumberFormat);
                double min = ((double)settings.loadMin * tempUser.Load);
                double max = ((double)settings.loadMax * tempUser.Load);

                if (annualConflict && val != 0.0f)
                {
                    row.Cells[column].BackColor = settings.c_hasAnnual;
                }
                else if (val > max)
                {
                    row.Cells[column].BackColor = settings.c_overUte;
                }
                else if (val < min)
                {
                    row.Cells[column].BackColor = settings.c_underUte;
                }
                else
                {
                    row.Cells[column].BackColor = settings.c_goodUte;
                }
            }
            GridView_Instances.UpdateRow(row.RowIndex, false);
        }

        protected void GridView_Instances_RowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            e.Row.Cells[0].Visible = false; //COMMENT THIS TO SHOW ID
            int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]);
            int start = e.Row.RowIndex == -1 ? 1 : (mode < 2 ? 2 : 3);
            int end = e.Row.RowIndex == -1 ? 2 : e.Row.Cells.Count;

            if (e.Row.Cells.Count > 1)
            {
                for (int i = start; i < end; i++)
                {
                    TableCell cell = e.Row.Cells[i]; //CSS
                    cell.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.borderWidth='3px';this.originalcolor=this.style.borderColor;this.style.borderColor='purple';";
                    cell.Attributes["onmouseout"] = "this.style.textDecoration='none'; this.style.borderWidth='1px';this.style.borderColor=this.originalcolor;";
                    cell.Attributes["onclick"] = string.Format("document.getElementById('{0}').value = {1}; {2}"
                       , SelectedGridCellIndex.ClientID, i
                       , Page.ClientScript.GetPostBackClientHyperlink((GridView)sender, string.Format("Select${0}", e.Row.RowIndex)));
                }
            }
        }

        protected void GridView_Instances_SelectedIndexChanged(object sender, EventArgs e)
        {
            var grid = (GridView)sender;
            int rIndex = grid.SelectedIndex;
            int cIndex = int.Parse(this.SelectedGridCellIndex.Value);
            if (!(rIndex < 0 || cIndex < 2))
            {
                int Subject_ID = Convert.ToInt32(grid.Rows[rIndex].Cells[0].Text);
                string Subject_Name = grid.Rows[rIndex].Cells[1].Text;
                openPopup(Subject_ID, Subject_Name, cIndex, rIndex);
            }else if (rIndex == -1 && cIndex == 1)//Sort
            {
                sortGridView(false);
            }
        }

        private void sortGridView(bool init)
        {
            DataTable tempDT = (DataTable)Session["INSTANCEMANAGER_dt"];
            DataView sortedView = new DataView(tempDT);
            string sort = ViewState["Sort"].ToString();
            int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]);
            if (!init)
            {
               sort = sort == "ASC" ? "DESC" : "ASC";
               ViewState["Sort"] = sort;
                SQLHandler sql = new SQLHandler();
                sql.editUserSort(Convert.ToInt32(Session["UserID"].ToString()), sort);
            }
            sortedView.Sort = (mode < 2 ? "Subject Name" : "Username") + " " + sort;
            GridView_Instances.DataSource = sortedView;
            GridView_Instances.DataBind();
        }

        private void openPopup(int subjectID,string subjectName, int column, int row)
        {
            int startYear = Convert.ToInt32(Session["INSTANCEMANAGER_startYear"]);
            int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]);
            short year = Convert.ToInt16(startYear + ((column - 2) / 12));
            byte month = Convert.ToByte((column - 1) - ((year - startYear) * 12));
            if (mode < 2) //Teach or Developer
            {
                stateMachine.Set(States.ADDING);
                clearInstancePopupControls();
                if (mode == 0)
                {
                    Popup_Instance_Textbox_Students.Visible = true;
                    Popup_Instance_Textbox_Students.Enabled = true;
                    Popup_Instance_Label_Students.Visible = true;
                    Popup_Instance_Div.Visible = true;
                }
                else
                {
                    Popup_Instance_Textbox_Students.Visible = false;
                    Popup_Instance_Textbox_Students.Enabled = false;
                    Popup_Instance_Label_Students.Visible = false;
                    Popup_Instance_Div.Visible = false;
                }

                Subject_Instance tempInstance = instanceSorter.getInstance(mode, year, subjectID, month);
                ModalPopupExtender1.Show();
                bool newInstance = (tempInstance == null);
                Session["INSTANCEMANAGER_POPUP_META"] = (newInstance ? "" : tempInstance.SubjectInstanceID.ToString()) + "," + subjectID + "," + month + "," + year + "," + column + "," + row;
                Popup_Instance_Label_Title.Text = subjectName + " - " + month + "/" + year;
                Popup_Instance_User_Label_Title.Text = subjectName + " - " + month + "/" + year;

                if (tempInstance == null) //new Instance
                {
                    Popup_Instance_Button_DeleteInstance.Enabled = false;




                }
                else //edit Instance
                {
                    foreach (Lecturer l in tempInstance.lecturers)
                    {
                        User tempUser = userStore[Lists.getObjectIndex(userStore, l.userID)];
                        Subject_Type tempSType = sTypeStore[Lists.getObjectIndex(sTypeStore, l.subjectTypeID)];
                        Popup_Instance_Listbox_AddedUsers.Items.Add(tempUser.Username + "|" + tempSType.Name + (!tempUser.isAvail(new DateTime(Convert.ToInt32(year),Convert.ToInt32(month),1)) ? "| !Annual Leave!" : ""));
                    }
                    if (Popup_Instance_Listbox_AddedUsers.Items.Count > 0)
                    {
                        Popup_Instance_Button_Edit.Enabled = true;
                        Popup_Instance_Button_Remove.Enabled = true;
                    }
                    Popup_Instance_Button_DeleteInstance.Enabled = true;
                    Popup_Instance_Textbox_Students.Text = tempInstance.Students.ToString();
                }
                Popup_Instance.Update();
            }
            else //Lecturer View
            {
                month--;
                if (month == 0)//crude
                {
                    month = 12;
                    year--;
                }
                Popup_Lecturer_Label_Title.Text = subjectName + " - " + month + "/" + year;
                InstanceCounter tempCounter = (InstanceCounter)instanceSorter.getLecInstance(year, Convert.ToInt32(subjectID) , month);

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[5] { new DataColumn("ID"), new DataColumn("Subject Name"), new DataColumn("Type"), new DataColumn("Month"), new DataColumn("Load") });
                foreach (Instance i in tempCounter.instances)
                {
                    Subject_Instance temp_sI = instanceSorter.sInstanceStore[Lists.getObjectIndex(instanceSorter.sInstanceStore, i.SubjectInstanceID)];
                    Subject temp_s = subjectStore[Lists.getObjectIndex(subjectStore, temp_sI.SubjectID)];
                    Subject_Type temp_sT = sTypeStore[Lists.getObjectIndex(sTypeStore, i.SubjectTypeID)];
                    string[] tokens = i.StartDate.Split('/');
                    int startMonth = (Convert.ToInt32(tokens[1])*12)+Convert.ToInt32(tokens[0]);
                    int thisMonth = ((int)year * 12) + month;

                    dt.Rows.Add(i.SubjectInstanceID.ToString(), temp_s.Name, temp_sT.Name, ((thisMonth- startMonth) + 1).ToString(), tempCounter.countInstance(i, sTypeStore));
                }
                Popup_Lecturer_GridView.DataSource = dt;
                Popup_Lecturer_GridView.DataBind();
                ModalPopupExtender2.Show();
                Popup_Lecturer.Update();
            }
        }

        private void clearInstancePopupControls()
        {
            Popup_Instance_Listbox_AddedUsers.Items.Clear();
            Popup_Instance_Error.Visible = false;
            Popup_Instance_Textbox_Students.Text = "";
            clearInstanceUserPopupControls();
        }
        private void clearInstanceUserPopupControls()
        {
            Popup_Instance_User_Listbox_SearchUsers.Items.Clear();
            Popup_Instance_User_TextBox_SearchUser.Text = "";
            Session["INSTANCEMANAGER_POPUP_EDIT"] = "";
            Session["INSTANCEMANAGER_POPUP_EDIT_ISStandard"] = false;
            Popup_Instance_User_Type.SelectedIndex = -1;
            Popup_Instance_User_Error.Visible = false;
        }

        protected void Button_Year1_Down_Click(object sender, EventArgs e)
        {
            setYearRange(0, false);
        }

        protected void Button_Year1_Up_Click(object sender, EventArgs e)
        {
            setYearRange(0, true);
        }

        protected void Button_Year2_Down_Click(object sender, EventArgs e)
        {
            setYearRange(1, false);
        }

        protected void Button_Year2_Up_Click(object sender, EventArgs e)
        {
            setYearRange(1, true);
        }

        protected void Popup_Instance_User_Button_Cancel_Click(object sender, EventArgs e)
        {
            Popup_Instance_Main.Visible = true;
            Popup_Instance_User.Visible = false;
            Popup_Instance_Delete.Visible = false;
            clearInstanceUserPopupControls();
        }

        public List<User> getLecturerUsers()
        {
            List<User> tempUsers = new List<User>();
            foreach (User u in userStore)
            {
                if (u.Privillege == 0)
                {
                    tempUsers.Add(u);
                }
            }
            return tempUsers;
        }

        public bool lecturerExists(string Username)
        {
            foreach (ListItem lI in Popup_Instance_Listbox_AddedUsers.Items)
            {
                string[] tokens = lI.Text.Split('|');
                if (tokens[0].Equals(Username))
                {
                    return true;
                }
                
            }
            return false;
        }

        public bool primaryLecturerExists()
        {
            foreach (ListItem lI in Popup_Instance_Listbox_AddedUsers.Items)
            {
                string[] tokens = lI.Text.Split('|');
                if (tokens[1].Equals("Standard"))
                {
                    return true;
                }

            }
            return false;
        }

        protected void Popup_Instance_User_Button_SearchUser_Click(object sender, EventArgs e)
        {
            string[] tokens = Session["INSTANCEMANAGER_POPUP_META"].ToString().Split(',');
            int subjectID = Convert.ToInt32(tokens[1]);
            int month = Convert.ToInt32(tokens[2]);
            int year = Convert.ToInt32(tokens[3]);
            string editUser = Session["INSTANCEMANAGER_POPUP_EDIT"].ToString();
            if (!Popup_Instance_User_TextBox_SearchUser.Text.Trim().Equals(""))
            {
                loadPopupUserSelector(getSearchList(getCompatibleLecturers(lecUserStore, subjectID, Popup_Instance_Listbox_AddedUsers, editUser), Popup_Instance_User_TextBox_SearchUser.Text.Trim()), new DateTime(year, month, 1));
            }
            else
            {
                loadPopupUserSelector(getCompatibleLecturers(lecUserStore, subjectID, Popup_Instance_Listbox_AddedUsers, editUser), new DateTime(year, month, 1));
            }
        }

        public static List<User> getSearchList(List<User> list, string value)
        {
            List<User> temp = new List<User>();
            foreach (User u in list)
            {
                if (u.ToString().ToLower().Contains(value.Trim().ToLower()))
                {
                    temp.Add(u);
                }
            }
            return temp;
        }

        protected void Popup_Instance_User_Button_Ok_Click(object sender, EventArgs e)
        {
            int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]);
            string editUser = Session["INSTANCEMANAGER_POPUP_EDIT"].ToString();
            bool ISStandard = false;
            if (!editUser.Equals(""))
            {
                ISStandard = (bool)Session["INSTANCEMANAGER_POPUP_EDIT_ISStandard"];
            }
            string err = "";

            if (Popup_Instance_User_Listbox_SearchUsers.SelectedIndex == -1)
            {
                err = Lists.addErrorText(err, "You must select a lecturer from the list!");
            }

            if (mode < 2 && !editUser.Equals("") && !ISStandard && primaryLecturerExists() && Popup_Instance_User_Type.SelectedValue.Equals("Standard"))
            {
                err = "There is allready a standard lecturer in this instance!";
            }
            if (mode < 2 && editUser.Equals("") && primaryLecturerExists() && Popup_Instance_User_Type.SelectedValue.Equals("Standard"))
            {
                err = "There is allready a standard lecturer in this instance!";
            }

            if (err.Length > 0)
            {
                Popup_Instance_User_Error.Visible = true;
                Popup_Instance_User_Error.Text = err;
                return;
            }


            Popup_Instance_Main.Visible = true;
            Popup_Instance_User.Visible = false;
            Popup_Instance_Delete.Visible = false;
            string text = Popup_Instance_User_Listbox_SearchUsers.SelectedValue.Split('|')[1] + "|" + Popup_Instance_User_Type.SelectedValue + (Popup_Instance_User_Listbox_SearchUsers.SelectedValue.Contains("!Annual") ? "| !Annual Leave!" : "");
            if (editUser.Equals(""))
            {
                Popup_Instance_Listbox_AddedUsers.Items.Add(text);
            }
            else
            {
                Popup_Instance_Listbox_AddedUsers.SelectedItem.Text = (text);
            }
            Popup_Instance_Button_Remove.Enabled = true;
            Popup_Instance_Button_Edit.Enabled = true;
            clearInstanceUserPopupControls();
        }

        protected void Popup_Instance_Button_Cancel_Click(object sender, EventArgs e)
        {
            clearInstancePopupControls();
            ModalPopupExtender1.Hide();
        }

        protected void Popup_Instance_Button_Ok_Click(object sender, EventArgs e)
        {
            if (stateMachine.STATE != States.ADDING)
            {
                return;
            }
            stateMachine.Set(States.STANDBY);
            string err = "";
            string[] tokens = Session["INSTANCEMANAGER_POPUP_META"].ToString().Split(',');
            bool newInstance = tokens[0].Equals("") ? true : false;
            Subject_Instance tempInstance = newInstance ? null : instanceSorter.sInstanceStore[Lists.getObjectIndex(instanceSorter.sInstanceStore, Convert.ToInt32(tokens[0]))];
            int subjectID = Convert.ToInt32(tokens[1]);
            byte month = Convert.ToByte(tokens[2]);
            short year = Convert.ToInt16(tokens[3]);
            int col = Convert.ToInt32(tokens[4]);
            int row = Convert.ToInt32(tokens[5]);
            bool mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]) == 0 ? true : false;
            string lecturers = "";
            string students = Popup_Instance_Textbox_Students.Text.Trim();
            short studentsNum = 0;

            if (mode)
            {
                try
                {
                    if (!students.Equals(""))
                    {
                        studentsNum = Convert.ToInt16(students);
                    }
                    else
                    {
                        err = Lists.addErrorText(err, "Please enter a student count!");
                    }
                }
                catch (Exception ex)
                {
                    err = Lists.addErrorText(err, "Student count must be a number!");
                }
            }
/*            if (Popup_Instance_Listbox_AddedUsers.Items.Count == 0)// Remove hardcoded lecturer limit
            {
                err = Lists.addErrorText(err, "There must be atleast one lecturer in this instance!");
            }else if (mode && !primaryLecturerExists())
            {
                err = Lists.addErrorText(err, "There must be a primary lecturer!");
            }*/

            if (err.Length > 0)
            {
                Popup_Instance_Error.Text = err;
                Popup_Instance_Error.Visible = true;
                return;
            }

            foreach (ListItem lI in Popup_Instance_Listbox_AddedUsers.Items)
            {
                if (lecturers.Length > 0)
                {
                    lecturers += ",";
                }
                string[] temp = lI.Text.Split('|');
                lecturers += (userStore[Lists.getObjectIndex(userStore, temp[0])].UserID + "|" + sTypeStore[Lists.getObjectIndex(sTypeStore,temp[1])].SubjectTypeID);
            }
            bool isChanged = false;
            if (!newInstance && tempInstance != null)
            {
                if (lecturers != tempInstance.lecturersString || studentsNum != tempInstance.Students)
                {
                    isChanged = true;
                }
            }


            SQLHandler sql = new SQLHandler();

            if (newInstance)
            {
                int maxid = sql.getMaxID("subjectinstances");
                sql.addInstance(maxid, mode, lecturers, month.ToString("00") + "/" + year.ToString("00"), subjectID, studentsNum);
                buildGridView(false);
                showMSG("Subject Instance", Popup_Instance_Label_Title.Text + " has been successfully added!");
            }
            else if (isChanged)
            {
                sql.editInstance(tempInstance.SubjectInstanceID, mode, lecturers, studentsNum);
                buildGridView(false);
                showMSG("Subject Instance", Popup_Instance_Label_Title.Text + " has been successfully modified!");
            }
            else
            {
                clearInstancePopupControls();
                ModalPopupExtender1.Hide();
            }

        }

        protected void GridView_Instances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Popup_Instance.Update();
        }

        protected void Button_Lecturer_Click(object sender, EventArgs e)
        {
            Session["INSTANCEMANAGER_mode"] = 2;
            updateViewButtons();
            buildGridView(false);
            string userSearch = (string)Session["INSTANCEMANAGER_userSearch"];
            search_Label.Text = "Lecturer Name: ";
            search_Textbox.Text = userSearch;
        }

        private void changeToSubjectSearch()
        {
            string subjectSearch = (string)Session["INSTANCEMANAGER_subjectSearch"];
            search_Label.Text = "Subject Name: ";
            search_Textbox.Text = subjectSearch;
        }

        protected void Button_Teaching_Click(object sender, EventArgs e)
        {
            Session["INSTANCEMANAGER_mode"] = 0;
            updateViewButtons();
            buildGridView(false);
            changeToSubjectSearch();
        }

        protected void Button_Developing_Click(object sender, EventArgs e)
        {
            Session["INSTANCEMANAGER_mode"] = 1;
            updateViewButtons();
            buildGridView(false);
            changeToSubjectSearch();
        }

        protected void GridView_Instances_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            UpdatePanel.Update();
        }

        protected void Popup_Instance_Button_Remove_Click(object sender, EventArgs e)
        {
            int index = Popup_Instance_Listbox_AddedUsers.SelectedIndex;
            if (index != -1)
            {
                Popup_Instance_Listbox_AddedUsers.Items.RemoveAt(index);
            }
            if (Popup_Instance_Listbox_AddedUsers.Items.Count == 0)
            {
                Popup_Instance_Button_Edit.Enabled = false;
                Popup_Instance_Button_Remove.Enabled = false;
            }
        }

        private void prepUserSelector()
        {
            string[] tokens = Session["INSTANCEMANAGER_POPUP_META"].ToString().Split(',');
            int subjectID = Convert.ToInt32(tokens[1]);
            int month = Convert.ToInt32(tokens[2]);
            int year = Convert.ToInt32(tokens[3]);
            string editUser = Session["INSTANCEMANAGER_POPUP_EDIT"].ToString();
            loadPopupUserSelector(getCompatibleLecturers(lecUserStore, subjectID, Popup_Instance_Listbox_AddedUsers, editUser), new DateTime(year, month, 1));
            Popup_Instance_Main.Visible = false;
            Popup_Instance_User.Visible = true;
            Popup_Instance_Delete.Visible = false;
            Popup_Instance_User_Type.Items.Clear();
            int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]);
            int min = mode == 0 ? 0 : 2;
            int max = mode == 0 ? 1 : sTypeStore.Count - 1;
            for (int i = min; i <= max; i++)
            {
                Popup_Instance_User_Type.Items.Add(sTypeStore[i].Name);
            }
        }


        protected void Popup_Instance_Button_Edit_Click(object sender, EventArgs e)
        {
            if (Popup_Instance_Listbox_AddedUsers.SelectedIndex != -1)
            {
                int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]);
                string[] tokens = Popup_Instance_Listbox_AddedUsers.SelectedValue.Split('|');
                string userName = tokens[0];
                bool Standard = tokens[1].Equals("Standard");
                Session["INSTANCEMANAGER_POPUP_EDIT"] = userName;
                Session["INSTANCEMANAGER_POPUP_EDIT_ISStandard"] = Standard;
                prepUserSelector();
                for (int i = 0; i < Popup_Instance_User_Listbox_SearchUsers.Items.Count; i++)
                {
                    if (Popup_Instance_User_Listbox_SearchUsers.Items[i].Text.Split('|')[1].Equals(userName))
                    {
                        Popup_Instance_User_Listbox_SearchUsers.SelectedIndex = i;
                    }
                }
                Popup_Instance_User_Type.SelectedValue = Popup_Instance_Listbox_AddedUsers.SelectedValue.Split('|')[1];
            }
        }

        protected void Popup_Instance_Button_Add_Click(object sender, EventArgs e)
        {
            int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]);
            prepUserSelector();
            if (mode == 0 && primaryLecturerExists())
            {
                Popup_Instance_User_Type.SelectedIndex = 1;
            }
        }

        private List<User> getCompatibleLecturers(List<User> lecUserStore, int subjectID, ListBox masterList, string edituser)
        {
            List<User> tempLecs = new List<User>();
            foreach (User u in lecUserStore)
            {
                foreach (int x in u.AbleSubjectList)
                {
                    if (x == subjectID)
                    {
                        bool exists = false;
                        foreach (ListItem lI in masterList.Items)
                        {
                            string username = lI.Text.Split('|')[0];
                            if (username == u.Username && username != edituser)
                            {
                                exists = true;
                            }
                        }
                        if (!exists)
                        {
                            tempLecs.Add(u);
                        }
                    }
                }
            }

            return tempLecs;
        }

        private void loadPopupUserSelector(List<User> compLecturers, DateTime startMonth)
        {
            string[] tokens = Session["INSTANCEMANAGER_POPUP_META"].ToString().Split(',');
            byte month = Convert.ToByte(tokens[2]);
            short year = Convert.ToInt16(tokens[3]);
            List<ListItem> list = new List<ListItem>();
            foreach (User u in compLecturers)
            {
                InstanceCounter iC = instanceSorter.getLecInstance(year, u.UserID, month);
                float total = 0.0f;
                if (iC != null)
                {
                    total = iC.getTotal(sTypeStore);
                }
                double max = ((double)u.Load * settings.loadMax);
                list.Add(new ListItem("("+(((double)(double)total/max)*100).ToString("0.00")+"%) "+total.ToString("0.00")+"/"+max.ToString("0.00") + (!u.isAvail(startMonth) ? " !Annual Leave! " : "") + " |" + u.Username));
            }
            list = list.OrderBy(x => x.Text).ToList<ListItem>();
            Popup_Instance_User_Listbox_SearchUsers.Items.Clear();
            Popup_Instance_User_Listbox_SearchUsers.Items.AddRange(list.ToArray<ListItem>());
        }

        protected void Popup_Instance_Button_DeleteInstance_Click(object sender, EventArgs e)
        {
            Popup_Instance_Main.Visible = false;
            Popup_Instance_User.Visible = false;
            Popup_Instance_Delete.Visible = true;
            Popup_Instance_Delete_Label.Text = "Are you sure you want to delete " + Popup_Instance_Label_Title.Text + "?";
        }

        protected void Popup_Instance_Delete_Button_Yes_Click(object sender, EventArgs e)
        {
            string[] tokens = Session["INSTANCEMANAGER_POPUP_META"].ToString().Split(',');
            if (!tokens[0].Equals("") ? true : false)
            {
                Subject_Instance tempInstance = instanceSorter.sInstanceStore[Lists.getObjectIndex(instanceSorter.sInstanceStore, Convert.ToInt32(tokens[0]))];

                if (tempInstance == null)
                {
                    return;
                }
                SQLHandler sql = new SQLHandler();
                sql.deleteRow("subjectinstances", Convert.ToInt32(tokens[0]));
                showMSG("Delete Instance", Popup_Instance_Label_Title.Text + " has been successfully deleted");
                buildGridView(false);
            }
        }

        private void showMSG(string title, string contents)
        {
            Popup_Instance_Main.Visible = false;
            Popup_Instance_User.Visible = false;
            Popup_Instance_Delete.Visible = false;

            Popup_Instance_messageBox_label_title.Text = title;
            Popup_Instance_messageBox_label_contents.Text = contents;
            Popup_Instance_messageBox.Visible = true;
        }

        protected void Popup_Instance_Delete_Button_No_Click(object sender, EventArgs e)
        {
            Popup_Instance_Main.Visible = true;
            Popup_Instance_User.Visible = false;
            Popup_Instance_Delete.Visible = false;
        }

        protected void Popup_Instance_messageBox_Button_Ok_Click(object sender, EventArgs e)
        {
            Popup_Instance_Main.Visible = true;
            Popup_Instance_messageBox.Visible = false;
            clearInstancePopupControls();
            ModalPopupExtender1.Hide();
        }

        protected void search_Button_Click(object sender, EventArgs e)
        {
            string search = search_Textbox.Text.Trim().ToLower();
            int mode = Convert.ToInt32(Session["INSTANCEMANAGER_mode"]);

            if (mode == 2)
            {
                if (search.Equals((string)Session["INSTANCEMANAGER_userSearch"])){
                    return;
                }
                Session["INSTANCEMANAGER_userSearch"] = search;
                if (search.Equals(""))
                {
                    Session["INSTANCEMANAGER_selectedUsers"] = selectedUsers = userStore;
                }
                else
                {
                    List<User> tempUsers = new List<User>();
                    foreach (User u in userStore)
                    {
                        if (u.Username.ToLower().Contains(search))
                        {
                            tempUsers.Add(u);
                        }
                    }
                    Session["INSTANCEMANAGER_selectedUsers"] = selectedUsers = tempUsers;
                }

            }
            else
            {
                if (search.Equals((string)Session["INSTANCEMANAGER_subjectSearch"])){
                    return;
                }
                Session["INSTANCEMANAGER_subjectSearch"] = search;
                if (search.Equals(""))
                {
                    Session["INSTANCEMANAGER_selectedSubjects"] = selectedSubjects = subjectStore;
                }
                else
                {
                    List<Subject> tempSubjects = new List<Subject>();
                    foreach (Subject s in subjectStore)
                    {
                        if ((s.UnitID+"-"+s.Name).ToLower().Contains(search)) ///NEED TO CHANGE IF A DYNAMIC SUBJECT COLOUMN IS MADE
                        {
                            tempSubjects.Add(s);
                        }
                    }
                    Session["INSTANCEMANAGER_selectedSubjects"] = selectedSubjects = tempSubjects;
                }
            }
            buildGridView(false);
        }

        private void FillLegend(int mode)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("Key"), new DataColumn("Value") });
            if (mode < 2)//dev and teach mode
            {
                dt.Rows.Add("1", "Annual Leave Conflict");
                dt.Rows.Add("2", "Good Instance");
                dt.Rows.Add("3", "No Lecturers");
                dt.Rows.Add("4", "No Primary Lecturer");
            }
            else//lecturer mode
            {
                dt.Rows.Add("1", "Annual Leave Conflict");
                dt.Rows.Add("5", "Over Utilize");
                dt.Rows.Add("6", "Good Utilize");
                dt.Rows.Add("7", "Under Utilize");
            }
            GridView_Legend.DataSource = dt;
            GridView_Legend.DataBind();
        }

        protected void GridView_Legend_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow gVR in GridView_Legend.Rows)
            {
                Color c = GetLegendIndexColour(Convert.ToInt32(gVR.Cells[0].Text));
                gVR.Cells[0].BackColor = c;
                gVR.Cells[0].ForeColor = c;
                gVR.Cells[0].Width = new Unit("30px");
            }
        }

        public Color GetLegendIndexColour(int index)
        {
            switch (index)
            {
                case 1:
                    return settings.c_hasAnnual;
                case 2:
                    return settings.c_Ok;
                case 3:
                    return settings.c_NoLecturer;
                case 4:
                    return settings.c_NoPrimary;
                case 5:
                    return settings.c_overUte;
                case 6:
                    return settings.c_goodUte;
                case 7:
                    return settings.c_underUte;
            }
            return Color.Red;
        }

        protected void LinkButton_Legend_Click(object sender, EventArgs e)
        {
            GridView_Legend.Visible = !GridView_Legend.Visible;
        }

        protected void Popup_Lecturer_Button_Ok_Click(object sender, EventArgs e)
        {
            ModalPopupExtender2.Hide();
        }

        protected void Popup_Lecturer_GridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[0].Visible = false;
        }

        protected void Popup_Lecturer_GridView_DataBound(object sender, EventArgs e)
        {
            Popup_Lecturer_GridView.HeaderRow.Cells[0].Visible = false;
        }
    }
}