﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using IndustryProject3A.Classes;

// @Author Luke Dunning

namespace IndustryProject3A
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        InstanceSorter instanceSorter;
        List<User> userStore = new List<User>();
        List<Subject> subjectStore = new List<Subject>();
        List<Subject> selectedSubjects = new List<Subject>();
        List<Subject_Type> sTypeStore = new List<Subject_Type>();
        Settings settings;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label_Username.Text = (string)Session["Username"];
                SQLHandler sql = new SQLHandler();
                subjectStore = selectedSubjects = sql.getSubjects();
                userStore = sql.getUsers();
                sTypeStore = sql.getSubjectTypes();
                settings = sql.getSettings();
                instanceSorter = new InstanceSorter(sql.getSubjectInstances());
                Session["Calendar_instanceSorter"] = instanceSorter;
                Session["Calendar_userStore"] = userStore;
                Session["Calendar_sTypeStore"] = sTypeStore;
                Session["Calendar_subjectStore"] = subjectStore;
                Session["Calendar_selectedSubjects"] = selectedSubjects;
                Session["Calendar_settings"] = settings;
                int uIndex;
                Session["Calendar_userIndex"] = uIndex = Lists.getObjectIndex(userStore, (string)Session["Username"]);
                ViewState["Sort"] = userStore[uIndex].Sort;
                ViewState["yearSelect_l"] = userStore[uIndex].lYear == 0 ? instanceSorter.teach_lYear : userStore[uIndex].lYear;
                ViewState["yearSelect_h"] = userStore[uIndex].hYear == 0 ? instanceSorter.teach_hYear : userStore[uIndex].hYear;
                FillLegend();
                if (instanceSorter.sInstanceStore.Count == 0 || subjectStore.Count == 0)
                {
                    Label_error.Text = "You have no rostered instances";
                    UpdatePanel.Visible = false;
                    return;
                }

                updateYearRangeButtons();
                buildGridView(true);
            }
            else
            {
                userStore = (List<User>)Session["Calendar_userStore"];
                subjectStore = (List<Subject>)Session["Calendar_subjectStore"];
                selectedSubjects = (List<Subject>)Session["Calendar_selectedSubjects"];
                instanceSorter = (InstanceSorter)Session["Calendar_instanceSorter"];
                sTypeStore = (List<Subject_Type>)Session["Calendar_sTypeStore"];
                settings = (Settings)Session["Calendar_settings"];
            }
        }

        private void updateYearRangeButtons()
        {
            int lYear = Convert.ToInt32(ViewState["yearSelect_l"]);
            int hYear = Convert.ToInt32(ViewState["yearSelect_h"]);

            if (lYear == hYear)
            {
                Button_Year2_Down.Visible = false;
                Button_Year2_Down.Enabled = false;

                Button_Year1_Up.Visible = false;
                Button_Year1_Up.Enabled = false;
            }
            else
            {
                Button_Year2_Down.Visible = true;
                Button_Year2_Down.Enabled = true;

                Button_Year1_Up.Visible = true;
                Button_Year1_Up.Enabled = true;
            }
        }

        private void setYearRange(int yearIndex, bool up)
        {
            int year = yearIndex == 0 ? Convert.ToInt32(ViewState["yearSelect_l"]) : Convert.ToInt32(ViewState["yearSelect_h"]);
            year = up ? year + 1 : year - 1;
            if (yearIndex == 0)
            {
                ViewState["yearSelect_l"] = year;
            }
            else
            {
                ViewState["yearSelect_h"] = year;
            }
            SQLHandler sql = new SQLHandler();
            sql.editUserYearSelect(Convert.ToInt32(Session["UserID"]), ViewState["yearSelect_l"].ToString() + "," + ViewState["yearSelect_h"].ToString());
            updateYearRangeButtons();
            buildGridView(false);
        }

        private void buildGridView(bool init)
        {
            if (!init)
            {
                SQLHandler sql = new SQLHandler();
                instanceSorter = new InstanceSorter(sql.getSubjectInstances());
                Session["Calendar_instanceSorter"] = instanceSorter;
            }
            int userID = Convert.ToInt32(Session["UserID"]);
            short yearStart = Convert.ToInt16(ViewState["yearSelect_l"]);
            short yearEnd = Convert.ToInt16(ViewState["yearSelect_h"]);

            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("ID"), new DataColumn("Subject Name") });
            for (int i = yearStart; i <= yearEnd; i++)
            {
                string yearTag = i.ToString().Substring(2, 2);
                dt.Columns.AddRange(new DataColumn[12] { new DataColumn("Jan/" + yearTag), new DataColumn("Feb/" + yearTag), new DataColumn("Mar/" + yearTag), new DataColumn("Apr/" + yearTag), new DataColumn("May/" + yearTag), new DataColumn("Jun/" + yearTag), new DataColumn("Jul/" + yearTag), new DataColumn("Aug/" + yearTag), new DataColumn("Sep/" + yearTag), new DataColumn("Oct/" + yearTag), new DataColumn("Nov/" + yearTag), new DataColumn("Dec/" + yearTag) });

            }
            int rowindex = 0;

            foreach (Subject s in selectedSubjects)
            {
                for (int m = 0; m < 2; m++)
                {
                    List<String> instanceInfo = new List<String>();
                    bool hasRecord = false;
                    for (short x = yearStart; x <= yearEnd; x++)
                    {
                        for (int i = 1; i <= 12; i++)
                        {
                            byte month = Convert.ToByte(i);
                            string info = "";

                            Subject_Instance tempInstance = (Subject_Instance)instanceSorter.getInstance(m, x, s.SubjectID, month);
                            if (tempInstance != null && tempInstance.containtsLecturer(userID))
                            {
                                info = "~";
                                hasRecord = true;
                            }
                            instanceInfo.Add(info);
                        }
                    }
                    if (hasRecord)
                    {
                        dt.Rows.Add(s.SubjectID.ToString(), (m == 1 ? "[D]" : "[T]") + s.UnitID + "-" + s.Name);
                        for (int k = 0; k < instanceInfo.Count; k++)
                        {
                            dt.Rows[rowindex][k + 2] = instanceInfo[k];
                        }
                        rowindex++;
                    }
                }

                Session["Calendar_startYear"] = yearStart;
                Session["Calendar_dt"] = dt;
                GridView_Instances.DataSource = dt;
                GridView_Instances.DataBind();
                sortGridView(true);
                Label_Year.Text = (yearStart == yearEnd) ? yearStart.ToString() : yearStart.ToString() + " - " + yearEnd.ToString();
                UpdatePanel.Update();
            }
        }

        private void sortGridView(bool init)
        {
            DataTable tempDT = (DataTable)Session["Calendar_dt"];
            DataView sortedView = new DataView(tempDT);
            string sort = ViewState["Sort"].ToString();

            if (!init)
            {
                sort = sort == "ASC" ? "DESC" : "ASC";
                ViewState["Sort"] = sort;
                SQLHandler sql = new SQLHandler();
                sql.editUserSort(Convert.ToInt32(Session["UserID"].ToString()), sort);
            }
            sortedView.Sort = "Subject Name" + " " + sort;
            GridView_Instances.DataSource = sortedView;
            GridView_Instances.DataBind();
            UpdatePanel.Update();
        }


        protected void GridView_Instances_RowCreated(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            //e.Row.Cells[0].Visible = false; //COMMENT THIS TO SHOW ID
            int start = e.Row.RowIndex == -1 ? 1 : 2;
            int end = e.Row.RowIndex == -1 ? 2 : e.Row.Cells.Count;
            if (e.Row.Cells.Count > 1)
            {
                for (int i = start; i < end; i++)
                {
                    TableCell cell = e.Row.Cells[i]; //CSS
                    cell.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.borderWidth='3px';this.originalcolor=this.style.borderColor;this.style.borderColor='purple';";
                    cell.Attributes["onmouseout"] = "this.style.textDecoration='none'; this.style.borderWidth='1px';this.style.borderColor=this.originalcolor;";
                    cell.Attributes["onclick"] = string.Format("document.getElementById('{0}').value = {1}; {2}"
                       , SelectedGridCellIndex.ClientID, i
                       , Page.ClientScript.GetPostBackClientHyperlink((GridView)sender, string.Format("Select${0}", e.Row.RowIndex)));
                }
            }
        }

        protected void GridView_Instances_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {

        }

        protected void Button_Year1_Down_Click(object sender, EventArgs e)
        {
            setYearRange(0, false);
        }

        protected void Button_Year1_Up_Click(object sender, EventArgs e)
        {
            setYearRange(0, true);
        }

        protected void Button_Year2_Down_Click(object sender, EventArgs e)
        {
            setYearRange(1, false);
        }

        protected void Button_Year2_Up_Click(object sender, EventArgs e)
        {
            setYearRange(1, true);
        }

        protected void GridView_Instances_DataBound(object sender, EventArgs e)
        {
            //GridView_Instances.HeaderRow.Cells[0].Visible = false; //COMMENT THIS TO SHOW ID

            foreach (GridViewRow gVR in GridView_Instances.Rows)
            {
                for (int i = 2; i < gVR.Cells.Count; i++)
                {
                    if (!gVR.Cells[i].Text.Equals("&nbsp;") && !gVR.Cells[i].Text.Equals(""))
                    {
                        updateCell(gVR, i);
                    }
                }
            }
        }

        private void updateCell(GridViewRow row, int column)
        {
            int startYear = Convert.ToInt32(Session["Calendar_startYear"]);
            short year = Convert.ToInt16(startYear + ((column - 2) / 12));
            byte month = Convert.ToByte((column - 1) - ((year - startYear) * 12));
            string text = row.Cells[column].Text;
            if (text.Equals("~"))
            {
                int mode = row.Cells[1].Text.ToCharArray()[1] == 'D' ? 1 : 0;
                int subjectID = Convert.ToInt32(row.Cells[0].Text);
                Subject_Instance tempInstance = instanceSorter.getInstance(mode, year, subjectID, month);

                if (tempInstance != null)
                {
                    if (mode == 0 && tempInstance.getPrimaryUserID() == Convert.ToInt32(Session["UserID"]))
                    {
                        row.Cells[column].BackColor = settings.c_teachLead;//Add settings
                    }
                    else if (mode == 1)
                    {
                        row.Cells[column].BackColor = settings.c_dev;
                    }
                    else
                    {
                        row.Cells[column].BackColor = settings.c_teachSupport;
                    }
                }
                row.Cells[column].Text = "";
            }
            UpdatePanel.Update();
        }

        protected void GridView_Instances_SelectedIndexChanged(object sender, EventArgs e)
        {
            var grid = (GridView)sender;
            int rIndex = grid.SelectedIndex;
            int cIndex = int.Parse(this.SelectedGridCellIndex.Value);
            if (!(rIndex < 0 || cIndex < 2))
            {
                int Subject_ID = Convert.ToInt32(grid.Rows[rIndex].Cells[0].Text);
                string Subject_Name = grid.Rows[rIndex].Cells[1].Text;
                int mode = grid.Rows[rIndex].Cells[1].Text.ToCharArray()[1] == 'D' ? 1 : 0;
                int startYear = Convert.ToInt32(Session["Calendar_startYear"]);
                short year = Convert.ToInt16(startYear + ((cIndex - 2) / 12));
                byte month = Convert.ToByte((cIndex - 1) - ((year - startYear) * 12));
                Subject_Instance tempInstance = instanceSorter.getInstance(mode, year, Subject_ID, month);
                if (tempInstance != null)
                {
                    openPopup(tempInstance, Subject_Name);
                }
            }
            else if (rIndex == -1 && cIndex == 1)//Sort
            {
                sortGridView(false);
            }
        }

        private void openPopup(Subject_Instance instance, string subjectName) //Open popup
        {
            ModalPopupExtender1.Show();
            iViewer_Label_Title.Text = subjectName.Split(']')[1] + " " + instance.Month + "/" + instance.Year;
            string students = (instance.mode == false ? "" : "Students: "+instance.Students.ToString());
            iViewer_Label_Students.Text = students;
            iViewer_Listbox.Items.Clear();
            foreach (Lecturer l in instance.lecturers)
            {
                User tempUser = userStore[Lists.getObjectIndex(userStore, l.userID)];
                Subject_Type tempSType = sTypeStore[Lists.getObjectIndex(sTypeStore, l.subjectTypeID)];
                iViewer_Listbox.Items.Add(tempUser.Username + "|" + tempSType.Name);
            }
            UpdatePanel_Popup.Update();
        }

        protected void iViewer_Button_Ok_Click(object sender, EventArgs e)
        {
            ModalPopupExtender1.Hide();
        }

        protected void LinkButton_Legend_Click(object sender, EventArgs e)
        {
            GridView_Legend.Visible = !GridView_Legend.Visible;
        }

        public Color GetLegendIndexColour(int index) // hard coded colour settings [CHANGE THIS IF YOU ADD NEW COLOUR SETTINGS]
        {
            switch (index)
            {
                case 1:
                    return settings.c_teachLead;
                case 2:
                    return settings.c_teachSupport;
                case 3:
                    return settings.c_dev;
            }
            return Color.Red;
        }

        protected void GridView_Legend_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow gVR in GridView_Legend.Rows)
            {
                Color c = GetLegendIndexColour(Convert.ToInt32(gVR.Cells[0].Text));
                gVR.Cells[0].BackColor = c;
                gVR.Cells[0].ForeColor = c;
                gVR.Cells[0].Width = new Unit("30px");
            }
        }

        private void FillLegend() // Legend filler
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("Key"), new DataColumn("Value") });
            dt.Rows.Add("1", "Teaching Instance - Lead");
            dt.Rows.Add("2", "Teaching Instance - Support");
            dt.Rows.Add("3", "Developing Instance");
            GridView_Legend.DataSource = dt;
            GridView_Legend.DataBind();
        }
    }
}