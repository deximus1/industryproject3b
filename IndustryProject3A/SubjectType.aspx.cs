﻿using System;
using IndustryProject3A.Classes;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

// @Author Luke Dunning

namespace IndustryProject3A
{
    public partial class SubjectType : System.Web.UI.Page
    {

        List<Subject_Type> sTypeStore = new List<Subject_Type>();
        State stateMachine;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadSubjectTypes();
                Lists.loadListBox(sTypeStore, subjectTypeList);
                Session["State"] = stateMachine = new State();
            }
            else
            {
                sTypeStore = (List<Subject_Type>)Session["SUBJECTTYPE_sTypeStore"];
                stateMachine = (State)Session["State"];
            }
        }

        private void loadSubjectTypes()
        {
            SQLHandler sql = new SQLHandler();
            if (sTypeStore != null && sTypeStore.Count > 0)
            {
                sTypeStore.Clear();
            }
            sTypeStore = sql.getSubjectTypes();
            Session["SUBJECTTYPE_sTypeStore"] = sTypeStore;
        }


        protected void SearchButton_Click(object sender, EventArgs e)
        {
            if (!SearchBox.Text.Trim().Equals(""))
            {
                Lists.loadListBox(Lists.getSearchList(sTypeStore,SearchBox.Text.Trim()),subjectTypeList);
            }
            else
            {
                Lists.loadListBox(sTypeStore,subjectTypeList);
            }
            
        }

        protected void addSubjectType_Click(object sender, EventArgs e)
        {
            ModalPopupExtender2.Show();
            stateMachine.Set(States.ADDING);
        }

        protected void newSubjectType_Button_Cancel_Click(object sender, EventArgs e)
        {
            emptyAddSubjectTypeControls();
            ModalPopupExtender2.Hide();
        }

        private void emptyAddSubjectTypeControls()
        {
            newSubjectType_TextBox_Name.Text = "";
            newSubjectType_TextBox_Load.Text = "";
            newSubjectType_Label_error.Text = "";
            newSubjectType_Label_error.Visible = false;
        }

        private void emptyEditSubjectTypeControls()
        {
            editSubjectType_TextBox_Name.Text = "";
            editSubjectType_TextBox_Load.Text = "";
            editSubjectType_Label_error.Text = "";
            editSubjectType_Label_error.Visible = false;
        }

        protected void editSubjectType_Button_Cancel_Click(object sender, EventArgs e)
        {
            emptyEditSubjectTypeControls();
            ModalPopupExtender1.Hide();
        }

        protected void newSubjectType_Button_Submit_Click(object sender, EventArgs e)
        {
            if (stateMachine.STATE != States.ADDING)
            {
                return;
            }
            SQLHandler sql = new SQLHandler();
            string err = "";
            string name = newSubjectType_TextBox_Name.Text.Trim();
            string load = newSubjectType_TextBox_Load.Text.Trim();
            float convLoad = 0.0f;
            bool validConvert = false;
            if (name.Equals(""))
            {
                err = Lists.addErrorText(err, "Name must not be empty!");
            }
            else if (sql.isValueTaken("subjecttypes", "Name", name))
            {
                err = Lists.addErrorText(err, "Name is allready use!");
            }
            if (load.Equals(""))
            {
                err = Lists.addErrorText(err, "Load must not be empty!");
            }
            else
            {
                try
                {
                    convLoad = float.Parse(load, CultureInfo.InvariantCulture.NumberFormat);
                    validConvert = true;
                }
                catch (Exception ex)
                {
                    err = Lists.addErrorText(err, "Load must be a value between 0.1 and 1.0");
                }
                if ((validConvert) && !(convLoad >= 0.00f && convLoad <= 1.00))
                {
                    err = Lists.addErrorText(err, "Load must be a value between 0.1 and 1.0");
                }
            }
            if (err.Length > 0)
            {
                newSubjectType_Label_error.Text = err;
                newSubjectType_Label_error.Visible = true;
                ModalPopupExtender2.Show();
                return;
            }
            stateMachine.Set(States.STANDBY);

            int maxid = sql.getMaxID("subjecttypes");
            sql.addSubjectType(maxid, name, convLoad);
            newSubjectType_Label_error.Visible = false;
            loadSubjectTypes();
            Lists.loadListBox(sTypeStore,subjectTypeList);
            emptyAddSubjectTypeControls();
            UpdatePanel.Update();
            showMSG("New Subject Type", "ID: '"+ (maxid+1) + "' Name: '"+name+"' has been added sucessfully!");
        }

        protected void editSubjectType_Click(object sender, EventArgs e)
        {
            int index = Lists.getObjectIndex(sTypeStore, subjectTypeList.SelectedValue);
            if (index == -1)
            {
                return;
            }
            stateMachine.Set(States.EDITING);
            ModalPopupExtender1.Show();
            editSubjectType_TextBox_Name.Text = sTypeStore[index].Name;
            editSubjectType_TextBox_Load.Text = sTypeStore[index].Load.ToString("0.00");
        }

        protected void editSubjectType_Button_Submit_Click(object sender, EventArgs e)
        {
            if (stateMachine.STATE != States.EDITING)
            {
                return;
            }
            SQLHandler sql = new SQLHandler();
            string err = "";
            int index = Lists.getObjectIndex(sTypeStore, subjectTypeList.SelectedValue);
            string name = editSubjectType_TextBox_Name.Text.Trim();
            string load = editSubjectType_TextBox_Load.Text.Trim();
            float convLoad = 0.0f;
            bool nameChange = (!name.Equals(sTypeStore[index].Name));
            bool loadChange = (!load.Equals(sTypeStore[index].Load.ToString("0.00")));

            if (nameChange || loadChange)
            {
                if (name.Equals(""))
                {
                    err = Lists.addErrorText(err, "Name must not be empty!");
                }
                else if (nameChange && sql.isValueTaken("subjecttypes", "Name", name))
                {
                    err = Lists.addErrorText(err, "Name is allready use!");
                }else if(nameChange && (index == 0 || index == 1))
                {
                    err = Lists.addErrorText(err, "You cannot change the name of this type!");
                }
                if (load.Equals(""))
                {
                    err = Lists.addErrorText(err, "Load must not be empty!");
                }
                try
                {
                    convLoad = float.Parse(load, CultureInfo.InvariantCulture.NumberFormat);
                }
                catch (Exception ex)
                {
                    err = Lists.addErrorText(err, "Load must be a value between 0.1 and 1.0");
                }
                if (!(convLoad >= 0.00f && convLoad <= 1.00))
                {
                    err = Lists.addErrorText(err, "Load must be a value between 0.1 and 1.0");
                }
                if (err.Length > 0)
                {
                    editSubjectType_Label_error.Text = err;
                    editSubjectType_Label_error.Visible = true;
                    ModalPopupExtender1.Show();
                    return;
                }
                stateMachine.Set(States.STANDBY);
                sql.editSubjectType(sTypeStore[index].SubjectTypeID, name, convLoad);
                loadSubjectTypes();
                if (nameChange)
                {
                    Lists.loadListBox(sTypeStore, subjectTypeList);
                }
                showMSG("Edit Subject Type", "Changes made to ID: '" + sTypeStore[index].SubjectTypeID + " Name: '" + name + "' have been sucessfully saved!");
            }
            if (stateMachine.STATE == States.EDITING)
            {
                stateMachine.Set(States.STANDBY);
            }
            UpdatePanel.Update();
            editSubjectType_Label_error.Visible = false;
            emptyEditSubjectTypeControls();
        }

        protected void deleteSubjectType_Click(object sender, EventArgs e)
        {
            int index = Lists.getObjectIndex(sTypeStore, subjectTypeList.SelectedValue);
            if (index == -1)
            {
                return;
            }
            if (index == 0 || index == 1)
            {
                showMSG("Access Denied!", "You cannot delete this type!");
                return;
            }
            stateMachine.Set(States.DELETING);
            ModalPopupExtender3.Show();
            deleteSubjectType_Label.Text = "Are you sure you want to delete: " +sTypeStore[index].Name+"? WARNING this will remove all users from associated instances with this subject type!";
        }

        protected void deleteSubjectType_Button_Yes_Click(object sender, EventArgs e)
        {
            if (stateMachine.STATE != States.EDITING)
            {
                return;
            }
            stateMachine.Set(States.STANDBY);
            SQLHandler sql = new SQLHandler();
            int index = Lists.getObjectIndex(sTypeStore, subjectTypeList.SelectedValue);
            int id = sTypeStore[index].SubjectTypeID;
            string name = sTypeStore[index].Name;
            sql.deleteRow("subjecttypes", id);
            loadSubjectTypes();
            Lists.loadListBox(sTypeStore,subjectTypeList);

            List<Subject_Instance> sInstanceStore = sql.getSubjectInstances();
            foreach (Subject_Instance sI in sInstanceStore) //Removes all subject type associations from the  instance DB
            {
                bool modified = false;
                for (int i = 0; i < sI.lecturers.Count; i++)
                {
                    if (sI.lecturers[i].subjectTypeID == id)
                    {
                        sI.lecturers.RemoveAt(i);
                        modified = true;
                    }
                }
                if (modified)
                {
                    string newLecturers = "";
                    foreach (Lecturer lec in sI.lecturers)
                    {
                        if (newLecturers.Length > 0)
                        {
                            newLecturers += ",";
                        }
                        newLecturers += lec.userID + "|" + lec.subjectTypeID;
                    }
                    sql.editInstanceLecturers(sI.SubjectInstanceID, newLecturers);
                }
            }
            UpdatePanel.Update();
            showMSG("Delete Subect Type", "Subject Type - ID: '" + id + "' Name: '" + name + " and user instances associated with this ID have been deleted sucessfully!");
        }

        private void showMSG(string title, string text)
        {
            messageBox_label_title.Text = title;
            messageBox_label_contents.Text = text;
            ModalPopupExtender4.Show();
        }

    }
}