﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IndustryProject3A.Classes;

// @Author Luke Dunning

namespace IndustryProject3A
{
    public partial class Admin : System.Web.UI.Page
    {
        Settings settings;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SQLHandler sql = new SQLHandler();
                settings = sql.getSettings();
                Session["ADMIN_settings"] = settings;
                LoadControls();
            }
            else
            {
                settings = (Settings)Session["ADMIN_settings"];
            }
        }

        private void LoadControls() //loads controls values from settings class
        {
            TextBox_MinLoad.Text = settings.loadMin.ToString("0.00");
            TextBox_MaxLoad.Text = settings.loadMax.ToString("0.00");
            TextBox_AnnualConflict_Colour.Text = settings.ConvertColourtoString(settings.c_hasAnnual);
            TextBox_LecturerGoodUtilise_Colour.Text = settings.ConvertColourtoString(settings.c_goodUte);
            TextBox_LecturerOverUtilise_Colour.Text = settings.ConvertColourtoString(settings.c_overUte);
            TextBox_LecturerUnderUtilise_Colour.Text = settings.ConvertColourtoString(settings.c_underUte);
            TextBox_NoIssues_Colour.Text = settings.ConvertColourtoString(settings.c_Ok);
            TextBox_NoLecturer_Colour.Text = settings.ConvertColourtoString(settings.c_NoLecturer);
            TextBox_NoPrimaryLecturer_Colour.Text = settings.ConvertColourtoString(settings.c_NoPrimary);
            TextBox_teachLead.Text = settings.ConvertColourtoString(settings.c_teachLead);
            TextBox_teachSupport.Text = settings.ConvertColourtoString(settings.c_teachSupport);
            TextBox_dev.Text = settings.ConvertColourtoString(settings.c_dev);
            Label_Error.Visible = false;
        }

        private void SetColourPicker(TextBox textbox, Color colour) //unused
        {
            textbox.Text = settings.ConvertColourtoString(colour);
            textbox.ForeColor = colour;
        }

        protected void Button_Revert_Click(object sender, EventArgs e)
        {
            LoadControls();
        }

        protected void Button_Save_Click(object sender, EventArgs e)
        {
            Label_Error.Visible = false;
            string err = "";
            float min = 0.0f;
            float max = 0.0f;
            try//Error Checking
            {
                min = float.Parse(TextBox_MinLoad.Text, CultureInfo.InvariantCulture.NumberFormat);
                if (min < 0.1f)
                {
                    err = Lists.addErrorText(err, "Minimum load must be a number above 0.1!");
                }
            }
            catch (Exception ex)
            {
                err = Lists.addErrorText(err, "Minimum load must be a number above 0.1!");
            }
            try
            {
                max = float.Parse(TextBox_MaxLoad.Text, CultureInfo.InvariantCulture.NumberFormat);
                if (max < 0.1f)
                {
                    err = Lists.addErrorText(err, "Maximum load must be a number above 0.1!");
                }
            }
            catch (Exception ex)
            {
                err = Lists.addErrorText(err, "Maximum load must be a number above 0.1!");
            }
            if (min >= max)
            {
                err = Lists.addErrorText(err, "Minimum load must be lower than the Maximum load!");
            }
            if (err.Length > 0)
            {
                Label_Error.Text = err;
                Label_Error.Visible = true;
                return;
            }

            //Delegate used by the YesNoDailoug if the user presses yes
            Session["ADMIN_YESNO_method"] = new Action<float,float>(SaveSettings);
            Session["ADMIN_YESNO_args"] = new object[] { min,max};
            YesNoDialouge("Confirm Save", "Are you sure you want to save these settings?");
            
        }

        private void SaveSettings(float min, float max) //Packs all vars and saves whatever has been changed
        {
            settings.loadMin = min;
            settings.loadMax = max;
            settings.c_hasAnnual = settings.ConvertHexToColour(TextBox_AnnualConflict_Colour.Text);
            settings.c_goodUte = settings.ConvertHexToColour(TextBox_LecturerGoodUtilise_Colour.Text);
            settings.c_overUte = settings.ConvertHexToColour(TextBox_LecturerOverUtilise_Colour.Text);
            settings.c_underUte = settings.ConvertHexToColour(TextBox_LecturerUnderUtilise_Colour.Text);
            settings.c_Ok = settings.ConvertHexToColour(TextBox_NoIssues_Colour.Text);
            settings.c_NoLecturer = settings.ConvertHexToColour(TextBox_NoLecturer_Colour.Text);
            settings.c_NoPrimary = settings.ConvertHexToColour(TextBox_NoPrimaryLecturer_Colour.Text);
            settings.c_teachLead = settings.ConvertHexToColour(TextBox_teachLead.Text);
            settings.c_teachSupport = settings.ConvertHexToColour(TextBox_teachSupport.Text);
            settings.c_dev = settings.ConvertHexToColour(TextBox_dev.Text);

            SQLHandler sql = new SQLHandler();
            sql.saveSettings(settings);
            Session["ADMIN_settings"] = settings;
            Session["ADMIN_msg"] = new string[] { "Saved Settings", "Settings have been sucessfully changed!" };
        }

        public static object invokeMethod(Delegate method, params object[] args) //invoke deledate
        {
            return method.DynamicInvoke((object[])args);
        }

        private void YesNoDialouge(string title, string contents)
        {
            Popup_Yesno_Label_Title.Text = title;
            Popup_Yesno_Label_Contents.Text = contents;
            ModalPopupExtender1.Show();
        }

        private void ShowMSG(string title, string contents)
        {
            Popup_MSG_Label_Title.Text = title;
            Popup_MSG_Label_contents.Text = contents;
            ModalPopupExtender2.Show();
        }

        protected void Popup_Yesno_Button_Yes_Click(object sender, EventArgs e)
        {
            ModalPopupExtender1.Hide();
            invokeMethod((Delegate)Session["ADMIN_YESNO_method"], (object[])Session["ADMIN_YESNO_args"]); //invoke delegate
            ShowMSG(((string[])Session["ADMIN_msg"])[0], ((string[])Session["ADMIN_msg"])[1]); //show confirm message
        }

        private void deleteAllInstances() // the fabled method
        {
            SQLHandler sql = new SQLHandler();
            sql.deleteAllSubjectInstances();
            Session["ADMIN_msg"] = new string[] { "Deleted all Subject Instances", "All subject instances have been sucessfully deleted!" };
        }

        protected void Button_ClearAllInstances_Click(object sender, EventArgs e)
        {
            Session["ADMIN_YESNO_method"] = new Action(deleteAllInstances); //Delegate the fabled method
            Session["ADMIN_YESNO_args"] = null;
            YesNoDialouge("Delete All Subject Instances", "!WARNING! This will DELETE all Subject Instances from the database! You can not recover them once they are gone! Are you sure you want to do this?");
        }
    }
}