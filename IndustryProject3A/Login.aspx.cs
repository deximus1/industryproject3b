﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using IndustryProject3A.Classes;

// @Author Luke Dunning

namespace IndustryProject3A
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Login1.FailureText = "Invalid Credentials!";
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {

            string username = Login1.UserName.Trim();
            string password = Login1.Password.Trim();
            if (!username.Equals("") && !password.Equals(""))
            {
                SQLHandler sql = new SQLHandler();
                e.Authenticated = sql.tryLogin(username, password);
            }
        }

        protected void Login1_LoggingIn(object sender, LoginCancelEventArgs e)
        {
            //USERNAME FILTER
            //if (Login.Username.Trim().Count(x => x == '.') != 1)
            //{
            //    e.Cancel = true;
            //}
        }

        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
            if (Session["Privilege"].Equals("0"))
            {
                Response.Redirect("Calendar.aspx");
            }
            else if (Session["Privilege"].Equals("1"))
            {
                Response.Redirect("CreateInstance.aspx");
            }
            else if(Session["Privilege"].Equals("2"))
            {
                Response.Redirect("Admin.aspx");
            }

        }
    }
}